//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EF.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class ParametersGroups
    {
        public ParametersGroups()
        {
            this.Parameters = new HashSet<Parameters1>();
            this.Parameters1 = new HashSet<Parameters>();
        }
    
        public System.Guid ParameterGroupId { get; set; }
        public string ParametersGroupName { get; set; }
        public string ParametrGroupCode { get; set; }
    
        public virtual ICollection<Parameters1> Parameters { get; set; }
        public virtual ICollection<Parameters> Parameters1 { get; set; }
    }
}
