﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Repository
{
    public class ParametersGroupsRepository : IParametersGroupsRepository
    {
        public bool Add(ParameterGroup entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(ParameterGroup entity)
        {
            throw new NotImplementedException();
        }

        public ParameterGroup Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ParameterGroup> SelectAll()
        {
            List<ParameterGroup> result = new List<ParameterGroup>();

            using (var context = new InfoModelDBEntities())
            {
                List<ParametersGroups> dbParamGrCollection = context.ParametersGroups.Where(x => (x.ParameterGroupId != null)).ToList();
                foreach (ParametersGroups item in dbParamGrCollection)
                {
                    result.Add(Mapper.MapParameterGroups(item));
                }
            }

            return result;
        }

        public Task<IEnumerable<ParameterGroup>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public bool Update(ParameterGroup entity)
        {
            throw new NotImplementedException();
        }
    }
}
