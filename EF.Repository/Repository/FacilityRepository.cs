﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.Repository.Mappers;
using DataLayer;
using EF.Repository.Abstract.Common;

namespace EF.Repository.Repository
{
    public class FacilityRepository : IFacilityRepository
    {

        public IEnumerable<Facility> SelectAll()
        {
            List<Facility> result = new List<Facility>();
            //try
            //{
            using (var context = new InfoModelDBEntities())
            {
                List<Facilities> dbFacCollection = context.Facilities.Where(x => (x.FacilityId != null)).ToList();
                foreach (Facilities item in dbFacCollection)
                {
                    result.Add(Mapper.MapFacilities(item));
                }
            }

            //}
            //catch (Exception er)
            //{
            //    MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            return result;
        }

        public Facility Select(Guid id)
        {
            Facility result = new Facility();
            //try
            //{
            using (var context = new InfoModelDBEntities())
            {
                Facilities dbFac = context.Facilities.FirstOrDefault<Facilities>(x => (x.FacilityId == id));

                result = Mapper.MapFacilities(dbFac);

            }

            //}
            //catch (Exception er)
            //{
            //    MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            return result;
        }

        public bool Update(Facility entity)
        {
            throw new NotImplementedException();
        }

        public bool Add(Facility entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Facility entity)
        {
            throw new NotImplementedException();
        }


        public async Task<IEnumerable<Facility>> SelectAllAsync()
        {

            List<Facility> result = new List<Facility>();
            try
            {
                using (var context = new InfoModelDBEntities())
                {

                    List<Facilities> dbFacCollection = context.Facilities.Where(x => (x.FacilityId != null)).ToList();
                    foreach (Facilities item in dbFacCollection)
                    {
                        result.Add(await Mapper.MapFacilitiesAsync(item));
                    }
                }

            }
            catch (Exception er)
            {
                MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;

        }
    }
}
