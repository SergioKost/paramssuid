﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Repository
{
    public class StagesRepository : IStagesRepository
    {
        public bool Add(Stage entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Stage entity)
        {
            throw new NotImplementedException();
        }

        public Stage Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Stage> SelectAll()
        {
            List<Stage> result = new List<Stage>();
            using (var context = new InfoModelDBEntities())
            {
                List<Stages> dbStageCollection = context.Stages.Where(x => (x.StageId != null)).ToList();
                foreach (Stages item in dbStageCollection)
                {
                    result.Add(Mapper.MapStage(item));
                }
            }            
            return result;
        }

        public Task<IEnumerable<Stage>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public bool Update(Stage entity)
        {
            throw new NotImplementedException();
        }
    }
}
