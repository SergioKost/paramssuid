﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Mappers;


namespace EF.Repository.Repository
{
    public class MeasuresRepository : IMeasuresRepository
    {
        public IEnumerable<Measure> SelectAll()
        {
            List<Measure> result = new List<Measure>();
            //try
            //{
            using (var context = new InfoModelDBEntities())
            {
                List<Measures> dbMeasCollection = context.Measures.Where(x => (x.MeasureId != null)).ToList();
                foreach (Measures item in dbMeasCollection)
                {
                    result.Add(Mapper.MapMeasures(item));
                }
            }

            //}
            //catch (Exception er)
            //{
            //    MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            return result;
        }

        public Task<IEnumerable<Measure>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public Measure Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Measure entity)
        {
            throw new NotImplementedException();
        }

        public bool Add(Measure entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Measure entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Measure> SelectByParameterId(Guid parameterId)
        {
            List<Measure> result = new List<Measure>();

            using (var context = new InfoModelDBEntities())
            {

                //Получить MeasuresGroupId
                Guid? grId = context.Parameters.Where(x => x.ParameterId == parameterId).FirstOrDefault().MeasureGroupId;

                if (grId != null)
                {
                    string measGrName = context.MeasuresGroups.Where(x => x.MeasureGroupId == grId).FirstOrDefault().MeasureGroupName;
                    if (measGrName == "ALL")
                    {
                        List<Measures> dbMeasCollection = context.Measures.Where(x => (x.MeasureId != null)).ToList();
                        foreach (Measures item in dbMeasCollection)
                        {
                            result.Add(Mapper.MapMeasures(item));
                        }
                    }
                    else
                    {
                        List<Measures> dbMeasCollection = context.Measures.Where(x => (x.MeasureGroupId == grId)).ToList();
                        foreach (Measures item in dbMeasCollection)
                        {
                            result.Add(Mapper.MapMeasures(item));
                        }
                    }

                }
            }

            return result;
        }
    }
}
