﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using System.Windows.Forms;
using EF.Repository.Mappers;
using EF.Repository.Abstract.Common;

namespace EF.Repository.Repository
{
    public class ElementRepository : IElementRepository
    {

        public IEnumerable<Element> SelectAll()
        {
            throw new NotImplementedException();
        }

        public Element Select(Guid id)
        {
            Element result = new Element();
            try
            {
                using (var context = new InfoModelDBEntities())
                {
                    result = Mapper.MapElements(context.Elements.Where(x => (x.Id == id)).FirstOrDefault(), null);
                }

            }
            catch (Exception er)
            {
                MessageBox.Show(String.Format("Что-то пошло не так при загрузке Элементов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        public IEnumerable<Element> SelectBySystem(Guid systemId)
        {
            List<Element> result = new List<Element>();
            try
            {
                using (var context = new InfoModelDBEntities())
                {
                    List<Elements> res = context.Elements.Where(x => (x.SystemId == systemId)).ToList();
                    foreach (Elements el in res)
                    {
                        result.Add(Mapper.MapElements(el, null));
                    }
                }
            }
            catch (Exception er)
            {
                MessageBox.Show(String.Format("Что-то пошло не так при загрузке Элементов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        public void AddParam(Element entElement, ElementParam entElemParam)
        { }

        public void DelParam(Element entElement, ElementParam entElemParam) { }

        public bool Update(Element entity)
        {
            throw new NotImplementedException();
            //try
            //{
            //    using (InfoModelDBEntities1 context = new InfoModelDBEntities1())
            //    {
            //        var t = context.ElementsParameters.FirstOrDefault(x => (x.ElementParameterId == entity.Id));
            //        //t = Mapper.MapElementParam(entity);
            //        if (t != null)
            //        {
            //            if (entity.ListValue != null && entity.ListValue.Count > 0)
            //            {
            //                //Параметр списочный
            //                t.ParametrValue = entity.SubParamValue.Id.ToString();
            //            }
            //            else
            //            {
            //                //Строковый параметр
            //                t.ParametrValue = entity.Value;
            //            }
            //            context.SaveChanges();
            //        }

            //    }
            //    return true;
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}          
        }

        public bool Add(Element entity)
        {
            throw new NotImplementedException();
        }

        //Устанавливаем значение IsComplete
        public bool SetIsComplete(Element entity, string isComplete)
        {
            try
            {
                using (InfoModelDBEntities context = new InfoModelDBEntities())
                {
                    var t = context.Elements.FirstOrDefault(x => (x.Id == entity.Id));
                    if (t != null)
                    {
                        t.IsComplete = isComplete;
                        context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Element entity)
        {
            throw new NotImplementedException();
        }


        public Task<IEnumerable<Element>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }
    }
}
