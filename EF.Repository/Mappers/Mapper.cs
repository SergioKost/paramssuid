﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataLayer;
using DataLayer.Services;

namespace EF.Repository.Mappers
{
    public class Mapper
    {
        //-----------------------------------------------------------------ОБЪЕКТЫ
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Объекты)
        /// </summary>
        /// <param name="dbFac"></param>
        /// <returns></returns>  
        public static Facility MapFacilities(Facilities dbFac)
        {
            Facility result = new Facility();

            result.Id = dbFac.FacilityId;
            result.Code = dbFac.FacilityCode;
            result.Name = dbFac.FacilityName;
            result.ShortName = dbFac.FacilityShortName;

            if (dbFac.Complexes != null)
            {
                result.Complexes = new List<Complex>();
                foreach (Complexes dbCompl in dbFac.Complexes)
                {
                    result.Complexes.Add(MapComplexes(dbCompl, result));
                    //return result;
                }

            }
            return result;
        }

        public static async Task<Facility> MapFacilitiesAsync(Facilities dbFac)
        {
            Facility result = new Facility();
            return await Task.Run(() =>
            {
                result.Id = dbFac.FacilityId;
                result.Code = dbFac.FacilityCode;
                result.Name = dbFac.FacilityName;
                result.ShortName = dbFac.FacilityShortName;

                if (dbFac.Complexes != null)
                {
                    result.Complexes = new List<Complex>();
                    foreach (Complexes dbCompl in dbFac.Complexes)
                    {
                        result.Complexes.Add(MapComplexes(dbCompl, result));
                    }

                }
                return result;

            });
        }      



        /// <summary>
        /// Переводит сущность модели данных в сущность БД (Объекты)
        /// </summary>
        /// <param name="Fac"></param>
        /// <returns></returns>        
        public static Facilities MapFacility(Facility Fac)
        {
            Facilities dbResult = new Facilities();
            dbResult.FacilityId = Fac.Id;
            dbResult.FacilityCode = Fac.Code;
            dbResult.FacilityName = Fac.Name;
            dbResult.FacilityShortName = Fac.ShortName;

            if (Fac.Complexes != null)
            {
                dbResult.Complexes = new List<Complexes>();
                foreach (Complex compl in Fac.Complexes)
                {
                    dbResult.Complexes.Add(MapComplex(compl, dbResult));
                }

            }
            return dbResult;
        }

        //--------------------------------------------------------------------КОМПЛЕКСЫ
        /// <summary>
        /// Переводит сущность модели данных в сущность БД (Комплексы)
        /// </summary>
        /// <param name="compl"></param>
        /// <returns></returns> 
        private static Complexes MapComplex(Complex compl, Facilities dbFac)
        {
            Complexes dbComlex = new Complexes();
            dbComlex.ComplexId = compl.Id;
            dbComlex.ComplexName = compl.Name;
            dbComlex.ComplexSppCode = compl.SppCode;
            dbComlex.Facilities = dbFac;

            if (compl.Systems != null)
            {
                dbComlex.Systems = new List<Systems>();
                foreach (DataLayer.System sys in compl.Systems)
                {
                    dbComlex.Systems.Add(MapSystems(sys));
                }
            }

            return dbComlex;
        }

        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Комплексы)
        /// </summary>
        /// <param name="dbCompl"></param>
        /// <returns></returns>    
        private static Complex MapComplexes(Complexes dbCompl, Facility fac)
        {
            Complex result = new Complex();
            result.Id = dbCompl.ComplexId;
            result.Name = dbCompl.ComplexName;
            result.SppCode = dbCompl.ComplexSppCode;
            result.Code = dbCompl.ComplexCode;
            result.Facility = fac;
            if (dbCompl.Systems != null)
            {
                result.Systems = new List<DataLayer.System>();
                foreach (Systems dbSystem in dbCompl.Systems)
                {
                    result.Systems.Add(MapSystem(dbSystem, result));
                }

            }
            return result;
        }

        //--------------------------------------------------------------------СИСТЕМЫ
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Системы)
        /// </summary>
        /// <param name="dbSystem"></param>
        /// <returns></returns>   
        private static DataLayer.System MapSystem(Systems dbSystem, Complex complex)
        {
            DataLayer.System result = new DataLayer.System();
            result.Id = dbSystem.SystemId;
            result.Code = dbSystem.SystemCode;
            result.Description = dbSystem.Description;
            result.Name = dbSystem.SystemName;
            result.Complex = complex;
            //if (dbSystem.Elements != null)
            //{
            //    result.Elements = new List<Element>();
            //    foreach (Elements dbElem in dbSystem.Elements)
            //    {
            //        result.Elements.Add(MapElements(dbElem, result));
            //    }
            //}


            return result;
        }

        /// <summary>
        /// Переводит сущность модели данных в сущность БД (Системы)
        /// </summary>
        /// <param name="sys"></param>
        /// <returns></returns>
        private static Systems MapSystems(DataLayer.System sys)
        {
            throw new NotImplementedException();
        }


        //-----------------------------------------------------------------------ЭЛЕМЕНТЫ
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Элементы)
        /// </summary>
        /// <param name="dbElem"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static Element MapElements(Elements dbElem, DataLayer.System sys)
        {
            Element result = new Element();
            result.Id = dbElem.Id;
            result.Name = dbElem.Name;
            result.Characteristic = dbElem.Characteristics;
            result.Position = dbElem.Position;
            result.TypeName = dbElem.ElementTypes.TypeName;
            result.IsComplete = dbElem.IsComplete;

            if (sys != null)
            {
                result.System = sys;
                result.Facility = sys.Complex.Facility;
            }

            //if (dbElem.ElementTypes != null)
            //{
            //    result.TypeName = dbElem.ElementTypes.TypeName;
            //}

            //Обычные параметры - если код объекта НЕ содержится в списке, поставляемом DataProvider
            //if (dbElem.ElementsParameters != null)
            if (!DataProvider.GetFacilitiesCodesForBKTables()
                .Contains(dbElem.Systems.Complexes.Facilities.FacilityCode))
            {
                //result.ParamList = new System.Collections.ObjectModel.ObservableCollection<ElementParam>();
                foreach (ElementsParameters item in dbElem.ElementsParameters)
                {
                    ElementParam ep = new ElementParam();
                    ep.Description = item.Parameters.ParameterName;
                    ep.Id = item.ElementParameterId;
                    ep.ParameterId = item.ParameterId;
                    
                    //if (item.MeasureId != null)
                    //{
                    //    ep.MeasureId = item.MeasureId;
                    //}

                    if (item.Stages != null)
                    {
                        ep.Stage = MapStage(item.Stages);
                    }


                    if (item.Parameters.SubParameters != null && item.Parameters.SubParameters.Count > 0)
                    {
                        //Получаем список значений
                        ep.ListValue = new List<SubParam>();
                        foreach (SubParameters sParItem in item.Parameters.SubParameters.OrderBy(t => t.SubParameterName))
                        {
                            SubParam sP = new SubParam();
                            sP.Id = sParItem.SubParameterId;
                            sP.Name = sParItem.SubParameterName;
                            //sP.Description =
                            ep.ListValue.Add(sP);
                        }

                        //Получаем текущее значение
                        try
                        {
                            ep.SubParamValue = ep.ListValue.FirstOrDefault(r => (r.Id == Guid.Parse(item.ParametrValue)));
                        }
                        catch (Exception ex) { ep.Value = item.ParametrValue; }
                    }

                    else
                    {
                        ep.Value = item.ParametrValue;
                    }

                    if (item.Measures != null)
                    {
                        ep.Unit = item.Measures.MeasureAbbreviation;
                        ep.Measure = MapMeasures(item.Measures);
                    }
                    result.ParamList.Add(ep);
                }
            }

            //Параметры для БК
            //if (dbElem.ElementsParametersForBK != null)
            else
            {
                //result.ParamList = new System.Collections.ObjectModel.ObservableCollection<ElementParam>();
                foreach (ElementsParametersForBK item in dbElem.ElementsParametersForBK)
                {
                    ElementParam ep = new ElementParam();
                    ep.Description = item.Parameters.ParameterName;
                    ep.Id = item.ElementParameterId;

                    ep.ParameterId = item.ParameterId;

                    //if (item.MeasureId != null)
                    //{
                    //    ep.MeasureId = item.MeasureId;
                    // }


                    if (item.Stages != null)
                    {
                        ep.Stage = MapStage(item.Stages);
                    }



                    if (item.Parameters.SubParameters != null && item.Parameters.SubParameters.Count > 0)
                    {
                        //Получаем список значений
                        ep.ListValue = new List<SubParam>();
                        foreach (SubParameters1 sParItem in item.Parameters.SubParameters.OrderBy(t => t.SubParameterName))
                        {
                            ep.ListValue.Add(MapSubParameter(sParItem));
                        }

                        //Получаем текущее значение
                        try
                        {
                            ep.SubParamValue = ep.ListValue.FirstOrDefault(r => (r.Id == Guid.Parse(item.ParametrValue)));
                        }
                        catch (Exception ex) { ep.Value = item.ParametrValue; }
                    }
                    else
                    {
                        ep.Value = item.ParametrValue;
                    }

                    if (item.Measures_new != null)
                    {
                        ep.Unit = item.Measures_new.MeasureAbbreviation;
                        ep.Measure = MapMeasures(item.Measures_new);
                    }
                    result.ParamList.Add(ep);
                }
            }

            //Сортировка параметров
            var orderedParamlist = result.ParamList.OrderBy(x => x.Description);
            result.ParamList = new System.Collections.ObjectModel.ObservableCollection<ElementParam>(orderedParamlist);

            return result;
        }


        //-----------------------------------------------------------------------СТАДИИ
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Стадии)
        /// </summary>
        /// <param name="stages"></param>
        /// <returns></returns>
        public static Stage MapStage(Stages stage)
        {
            Stage result = new Stage();
            result.Id = stage.StageId;
            result.StageName = stage.StageName;
            result.StageDescription = stage.StageDescription;
            
            return result;
        }


        //-----------------------------------------------------------------------ПАРАМЕТРЫ ЭЛЕМЕНТОВ
        /// <summary>
        /// Переводит сущность модели данных в сущность БД (Параметры элементов)
        /// </summary>
        /// <param name="elParam"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static ElementsParameters MapElementParam(ElementParam elParam)
        {
            ElementsParameters dbElemParams = new ElementsParameters();
            dbElemParams.ElementParameterId = elParam.Id;
            dbElemParams.ParametrValue = elParam.Value;

            return dbElemParams;
        }

        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Параметры элементов)
        /// </summary>
        /// <param name="dbElemParam"></param>
        /// <returns></returns>
        public static ElementParam MapElementsParameters(ElementsParameters dbElemParam)
        {
            ElementParam elemParam = new ElementParam();
            elemParam.Id = dbElemParam.ElementId;


            return elemParam;
        }

        //-----------------------------------------------------------------------ПАРАМЕТРЫ ОТДЕЛЬНЫЕ
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Параметры)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Parametr MapParameters(Parameters item)
        {
            Parametr result = new Parametr();
            result.Id = item.ParameterId;
            result.Description = item.ParameterName;
            if (item.MeasureGroupId != null)
            {
                //result.MeasureGroupId = (Guid)item.MeasureGroupId;
                result.MeasureGroup = MapMeasureGroups(item.MeasuresGroups);
            }

            if (item.ParameterGroupId != null)
            {
                result.ParameterGroup = MapParameterGroups(item.ParametersGroups);
            }

            if (item.SubParameters != null && item.SubParameters.Count > 0)
            {
                //result.SubParameters = new List<SubParam>();
                foreach (SubParameters sParItem in item.SubParameters)
                {
                    SubParam sP = new SubParam();
                    sP.Id = sParItem.SubParameterId;
                    sP.Name = sParItem.SubParameterName;
                    //sP.Description = 
                    result.SubParameters.Add(sP);
                }

            }
            return result;
        }

        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Параметры для БК)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Parametr MapParameters1(Parameters1 item)
        {
            Parametr result = new Parametr();
            result.Id = item.ParameterId;
            result.Description = item.ParameterName;
            result.Code = item.ParameterCode;
            if (item.MeasureGroupId != null)
            {
                //result.MeasureGroupId = (Guid)item.MeasureGroupId;
                result.MeasureGroup = MapMeasureGroups(item.MeasuresGroups);
            }

            if (item.PatameterGroupId != null)
            {
                result.ParameterGroup = MapParameterGroups(item.ParametersGroups);
            }

            if (item.SubParameters != null && item.SubParameters.Count > 0)
            {
                //result.SubParameters = new List<SubParam>();
                foreach (SubParameters1 sParItem in item.SubParameters)
                {
                    result.SubParameters.Add(MapSubParameter(sParItem));
                }

            }
            return result;
        }

        /// <summary>
        /// Переводит сущность модели данных в сущность БД (Параметры для БК)
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal static Parameters1 MapParameters1(Parametr entity)
        {
            Parameters1 result = new Parameters1();
            result.ParameterId = entity.Id;
            result.ParameterName = entity.Description;
            result.ParameterCode = entity.Code;
            result.PatameterGroupId = entity.ParameterGroup.Id;
            result.MeasureGroupId = entity.MeasureGroup.Id;

            return result;
        }


        //-----------------------------------------------------------------------Единицы измрения
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Единицы измерения)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Measure MapMeasures(Measures item)
        {
            Measure result = new Measure();
            result.Id = item.MeasureId;
            result.Name = item.MeasureName;
            result.Ratio = item.MeasureRatio;
            result.Abbreviation = item.MeasureAbbreviation;
            return result;
        }

        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Единицы измерения для БК)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Measure MapMeasures(Measures1 item)
        {
            Measure result = new Measure();
            result.Id = item.MeasureId;
            result.Name = item.MeasureName;
            result.Ratio = item.MeasureRatio;
            result.Abbreviation = item.MeasureAbbreviation;
            result.Code = item.MeasureCode;

            if (item.MeasureGroupId != null)
            {
                result.MeasureGroupId = (Guid)item.MeasureGroupId;
                result.MeasureGroup = Mapper.MapMeasureGroups(item.MeasuresGroups);
            }
                       
            return result;
        }

        /// <summary>
        /// Переводит сущность модели данных в сущность БД(Единицы измерения для БК)
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal static Measures1 MapMeasures(Measure entity)
        {
            Measures1 result = new Measures1();
            result.MeasureId = entity.Id;
            result.MeasureName = entity.Name;
            result.MeasureRatio = entity.Ratio;
            result.MeasureAbbreviation = entity.Abbreviation;
            result.MeasureCode = entity.Code;
            result.MeasureGroupId = entity.MeasureGroupId;
            
            return result;
        }




        //-----------------------------------------------------------------------SupParameters
        /// <summary>
        /// Переводит сущность модели данных в сущность БД (SupParameters - БК)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal static SubParameters1 MapSubParameter(SubParam entity)
        {
            SubParameters1 result = new SubParameters1();
            result.SubParameterId = entity.Id;
            result.SubParameterName = entity.Name;
            result.ParameterId = entity.ParameterId;
            result.SubParameterCode = entity.Code;
            result.SubParameterDescription = entity.Description;
            result.SubParameterSource = entity.Source;
            return result;
        }



        /// <summary>
        /// /// Переводит сущность БД в сущность модели данных (SubParameters - БК)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal static SubParam MapSubParameter(SubParameters1 item)
        {
            SubParam sP = new SubParam();
            sP.Id = item.SubParameterId;
            sP.Name = item.SubParameterName;
            if (item.ParameterId != null)
            {
                sP.ParameterId = (Guid)item.ParameterId;
            }

            sP.Description = item.SubParameterDescription;
            sP.Source = item.SubParameterSource;
            sP.Code = item.SubParameterCode;

            return sP;
        }


        //-----------------------------------------------------------------------MeasureGroups
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (MeasureGroups)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal static MeasureGroup MapMeasureGroups(MeasuresGroups item)
        {
            MeasureGroup result = new MeasureGroup();
            result.Id = item.MeasureGroupId;
            result.Name = item.MeasureGroupName;
            return result;
        }
        /// <summary>
        ///  Переводит сущность модели данных в сущность БД(MeasureGroups)
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal static MeasuresGroups MapMeasureGroups(MeasureGroup entity)
        {
            MeasuresGroups result = new MeasuresGroups();
            result.MeasureGroupId = entity.Id;
            result.MeasureGroupName = entity.Name;
            return result;
        }


        //-----------------------------------------------------------------------ParameterGroups
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (ParameterGroup)
        /// </summary>
        /// <param name="parametersGroups"></param>
        /// <returns></returns>
        internal static ParameterGroup MapParameterGroups(ParametersGroups dbParametersGroups)
        {
            ParameterGroup result = new ParameterGroup();
            result.Id = dbParametersGroups.ParameterGroupId;
            result.Name = dbParametersGroups.ParametersGroupName;
            result.Code = dbParametersGroups.ParametrGroupCode;
            return result;
        }


        //-----------------------------------------------------------------------Users
        /// <summary>
        /// Переводит сущность БД в сущность модели данных (Users)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal static User MapUser(Users item)
        {
            User result = new User();
            result.Id = item.Id;
            result.Name = item.UserPrincipalName;
            result.FIO = item.FIO;
            result.Department = item.Department;
            result.Phone = item.Phone;
            result.Office = item.Office;
            result.IsAdmin = item.UserParamAdmin.Count > 0;
            return result;
        }
    }
}
