﻿using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Editors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParamsSUID.Services
{
    public class CustomEventArgsConverter : EventArgsConverterBase<ValidationEventArgs>
    {
        protected override object Convert(object sender, ValidationEventArgs args)
        {
            return args.Value;
        }
    }
}
