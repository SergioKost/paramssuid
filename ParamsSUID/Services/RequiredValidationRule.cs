﻿using EF.Repository.Abstract.Common;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ParamsSUID.Services
{
    public class RequiredValidationRule : ValidationRule
    {
        public string FieldName { get; set; }

        public ISubParamRepository rep { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty(value?.ToString())) // null or empty
                return new ValidationResult(false, "Поле не может быть пустым");            

            return ValidationResult.ValidResult;
        }
    }
}