﻿using DevExpress.Xpf.Editors.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ParamsSUID.Services
{
    public class InvalidValueBehaviorHelper : DependencyObject
    {
        public static readonly DependencyProperty InvalidValueBehaviorProperty;

        static InvalidValueBehaviorHelper()
        {
            InvalidValueBehaviorProperty = DependencyProperty.RegisterAttached("InvalidValueBehavior", typeof(InvalidValueBehavior), typeof(InvalidValueBehaviorHelper),
                new PropertyMetadata(InvalidValueBehavior.AllowLeaveEditor, null));
        }

        public static InvalidValueBehavior GetInvalidValueBehavior(DependencyObject d)
        {
            return (InvalidValueBehavior)d.GetValue(InvalidValueBehaviorProperty);
        }
        public static void SetInvalidValueBehavior(DependencyObject d, InvalidValueBehavior value)
        {
            d.SetValue(InvalidValueBehaviorProperty, value);
        }
    }
}
