﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataLayer;

namespace ParamsSUID.Services
{
    public class Comparator : IComparer
    {
        public int Compare(string x, string y)
        {

            //if (x is Parametr && y is Parametr)
            //{
            //    return String.Compare((x as Parametr).Description, (y as Parametr).Description);
            //}
            //else
            //{
            //    return String.Compare((x as Measure).Name, (y as Measure).Name);
            //}

            return 1;
        }

        public int Compare(object x, object y)
        {
            if (x is SubParam & y is SubParam)
            {
                Double xDouble, yDouble;

                if (Double.TryParse((x as SubParam).Name, out xDouble) & Double.TryParse((y as SubParam).Name, out yDouble))
                {
                    //сравниваем xDouble и yDouble как числа
                    if (xDouble == yDouble)
                    {
                        return 0;
                    }
                    if (xDouble < yDouble)
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }

                }
                else
                {
                    //сравниваем как строки
                    return String.Compare((x as SubParam).Name, (y as SubParam).Name);
                }
                
            }

            throw new NotImplementedException();

        }
    }
}
