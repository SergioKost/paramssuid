﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using ParamsSUID.Views;

namespace ParamsSUID.Services
{
    //Класс, обеспечивающий показ дочерних окон (не используется)
    public class WindowFactory : IWindowService
    {
        public void CreateEditWindow(EditWindowVM viewModel)
        {
            EditWindow newWindow = new EditWindow();
            newWindow.DataContext = viewModel;
            
            //Инициализация Action для последующего закрытия
            if (viewModel.CloseAction == null)
            {
                viewModel.CloseAction = new Action(() => newWindow.Close());
            }
            
            newWindow.SetFocus(viewModel.IsStringParam);
            newWindow.ShowDialog();
            
        }

        public void CreateAddParamWindow(AddWindowVM viewModel)
        {
            AddParamWindow newWindow = new AddParamWindow();
            //newWindow.DataContext = viewModel;
            //newWindow.ShowDialog();
        }
        

        public bool ShowDialogWindow(string text)
        {
            MessageBoxButton button = MessageBoxButton.YesNo;
            MessageBoxResult result = MessageBox.Show(text, "", button);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    return true;
                case MessageBoxResult.No:
                    return false;
                default:
                    return false;
            }
        }
               

        public void ShowMessageWindow(string text)
        {
            MessageBox.Show(text);
        }

        //Создание формы задания атрибута IsComplete
        public string CreateIsCompleteDialogWindow()
        {
            string result = null;
            IsCompleteDialogWindow isCompDialogWindow = new IsCompleteDialogWindow();

            ////Подписываемся на событие
            //isCompDialogWindow.SaveEvent += delegate (string x)
            //{
            //    result = x;
            //};

            isCompDialogWindow.ShowDialog();

            return result;
        }

    }
}
