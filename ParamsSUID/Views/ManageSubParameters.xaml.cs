﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewModel;

namespace ParamsSUID.Views
{
    /// <summary>
    /// Логика взаимодействия для ManageSubParameters.xaml
    /// </summary>
    public partial class ManageSubParameters : UserControl
    {
        public ManageSubParameters()
        {
            InitializeComponent();

            
            Messenger.Default.Register<AddSubParamWindowVM>(this, "ShowAddSubParamView", addWindowVM =>
            {
                var v = new AddSubParameterWindow()
                {
                    DataContext = addWindowVM,
                    //Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ShowInTaskbar = false
                };
                v.ShowDialog();
            });


            Messenger.Default.Register<EditSubParamWindowVM>(this, "ShowEditSubParamView", editWindowVM =>
            {
                var v = new AddSubParameterWindow()
                {
                    DataContext = editWindowVM,
                    //Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ShowInTaskbar = false
                };
                v.ShowDialog();
            });


        }
    }
}
