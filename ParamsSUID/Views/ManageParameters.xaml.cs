﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewModel;

namespace ParamsSUID.Views
{
    /// <summary>
    /// Логика взаимодействия для ManageParameters.xaml
    /// </summary>
    public partial class ManageParameters : UserControl
    {
        public ManageParameters()
        {
            InitializeComponent();

            Messenger.Default.Register<AddParameterWindowVM>(this, "ShowAddParameterView", addWindowVM =>
            {
                var v = new AddParameterWindow()
                {
                    DataContext = addWindowVM,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ShowInTaskbar = false
                };
                v.ShowDialog();
            });

            Messenger.Default.Register<EditParameterWindowVM>(this, "ShowEditParamView", editWindowVM =>
            {
                var v = new AddParameterWindow()
                {
                    DataContext = editWindowVM,
                    //Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ShowInTaskbar = false
                };
                v.ShowDialog();
            });
        }
    }
}
