﻿using GalaSoft.MvvmLight.Messaging;
using ParamsSUID.Services;
using System;
using System.Deployment.Application;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewModel;
using ViewModel.Logger;


namespace ParamsSUID.Views
{
    /// <summary>
    /// Логика взаимодействия для ManageParam.xaml
    /// </summary>
    public partial class ManageParam : UserControl
    {
        public ManageParam()
        {
            //try
            //{
                InitializeComponent();

            //    WindowFactory wFactory = new WindowFactory();
            //    ManageParamVM viewModel = new ManageParamVM(wFactory);

            //    //Инициализация Action для последующего сортировки
            //    if (viewModel.SortAction == null)
            //    {
            //        viewModel.SortAction = new Action(() => Sorting());
            //    }

            //    this.DataContext = viewModel;

            //}
            //catch (Exception e)
            //{
            //    //MessageBox.Show(e.Message);
            //    Logger.Error(e, "Error on ManageParam constructor");
            //    throw;
            //}



            Messenger.Default.Register<AddWindowVM>(this, "ShowAddView", addWindowVM => 
            {
                string stName = Properties.Settings.Default.DefaultStageName;
                addWindowVM.SetSelectedStage(stName);
                var v = new AddParamWindow
                {
                    DataContext = addWindowVM,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ShowInTaskbar = false
                };
                v.ShowDialog();
            });

            Messenger.Default.Register<EditWindowVM>(this, "ShowEditView", editWindowVM =>
            {
                
                var v = new EditWindow
                {
                    
                    //Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ShowInTaskbar = false
                };
                editWindowVM.CloseAction = new Action(() => v.Close());
                v.DataContext = editWindowVM;
                v.SetFocus(editWindowVM.IsStringParam);
                v.ShowDialog();
            });


            Messenger.Default.Register<NotificationMessage>(this, "ShowSetCompleteView", msg =>
            {
                IsCompleteDialogWindow isCompDialogWindow = new IsCompleteDialogWindow();

                //Подписываемся на событие
                //isCompDialogWindow.SaveEvent += delegate(string x)
                //{
                //    result = x;
                //};

                isCompDialogWindow.ShowDialog();
            });           

        }

        //Сортировка dataGrid2
        public void Sorting()
        {
            //Сортировка по свойству "Description" коллекции param.ListValue
            ManageParamVM vm = (ManageParamVM)this.DataContext;
            if (vm.SelectedElem != null && vm.SelectedElem.ParamList != null & vm.SelectedElem.ParamList.Count > 0)
            {
                System.ComponentModel.ICollectionView view = CollectionViewSource.GetDefaultView(vm.SelectedElem.ParamList);
                //стандартная сортировка
                view.SortDescriptions.Add(new System.ComponentModel.SortDescription("Description", System.ComponentModel.ListSortDirection.Ascending));

            }
        }


       

    }
}
