﻿using DataLayer;
using DevExpress.Xpf.Editors;
using ParamsSUID.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


using ViewModel;


namespace ParamsSUID.Views
{
    /// <summary>
    /// Логика взаимодействия для EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {

        public EditWindow()
        {
            InitializeComponent();
        }

        //Установка фокуса ввода на соотв. элемент формы
        internal void SetFocus(bool isTextBox )
        {
            if (isTextBox)
                textBox1.Focus();
            else
                //comboBox1.Focus();
                lookUpEdit1.Focus();
            
        }


        private void textBox1_Validate(object sender, ValidationEventArgs e)
        {
            e.IsValid = ElementParam.ValidateValue((string)e.Value);
            e.ErrorContent = ElementParam.Error;
        }

        //private void FilterTextEdit_KeyUp(object sender, KeyEventArgs e)
        //{
        //    CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(comboBox1.ItemsSource);            

        //    itemsViewOriginal.Filter = ((o) =>
        //    {
        //        if (String.IsNullOrEmpty(FilterTextEdit.Text)) return true;
        //        else
        //        {
        //            if (((SubParam)o).Name.Contains(FilterTextEdit.Text)) return true;
        //            else return false;
        //        }
        //    });

        //    itemsViewOriginal.Refresh();
        //}
    }
}
