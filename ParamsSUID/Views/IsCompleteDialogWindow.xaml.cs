﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParamsSUID.Views
{
    /// <summary>
    /// Логика взаимодействия для IsCompleteDialogWindow.xaml
    /// </summary>
    public partial class IsCompleteDialogWindow : Window
    {
        //public delegate void DelegateSaveEventHandler(string x);
        //public event DelegateSaveEventHandler SaveEvent;

        public IsCompleteDialogWindow()
        {
            InitializeComponent();
            
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            string messageText = null;
            if ((bool)this.No.IsChecked)
            {
                //SaveEvent("нет");
                messageText = "нет";
            }
            if ((bool)this.Null.IsChecked)
            {
                //SaveEvent(null);
            }

            if ((bool)this.Yes.IsChecked)
            {
                //SaveEvent("да");
                messageText = "да";
            }

            var msg = new NotificationMessage(messageText);
            Messenger.Default.Send<NotificationMessage>(msg, "ReturnedSetCompleteValue");

            this.Close();
        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
