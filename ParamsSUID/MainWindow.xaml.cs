﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using EF.Repository.Repository;
using GalaSoft.MvvmLight.Messaging;
using ParamsSUID.Services;
using ParamsSUID.Views;
using ViewModel;
using ViewModel.Logger;
//using DataLayer;

namespace ParamsSUID
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            InitializeComponent();
            Logger.Info("Application starting...");
            Logger.Info(String.Format("User: {0}", Environment.UserName));

            MainWindowVM viewModel = new MainWindowVM(new DialogService());
            DataContext = viewModel;

            Messenger.Default.Register<UserSettingsVM>(this, "ShowSettingsWindow", userSetVM =>
            {
                UserSettingsWindow userSettingsWindow = new UserSettingsWindow();
                userSetVM.StageName = Properties.Settings.Default.DefaultStageName.ToString();
                userSetVM.CloseAction = new Action(() => userSettingsWindow.Close());
                userSetVM.SavePropStageAction = new Action<string>((stageName) => userSettingsWindow.SavePropStage(stageName));
                userSettingsWindow.DataContext = userSetVM;
                userSettingsWindow.ShowDialog();
            });


        }  

        private void Window_Closed(object sender, EventArgs e)
        {
            Logger.Info("Application closed");            
        }

        private void CloseCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        
    }
}
