﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Element : Entity
    {
        public string Name { get; set; }

        public string Position { get; set; }

        public string Characteristic { get; set; }
        

        public virtual System System { get; set; }

        public virtual Facility Facility { get; set; }

        public ObservableCollection<ElementParam> ParamList { get; set; }

        public string TypeName { get; set; }

        public bool HasParams { get; set; }

        public bool IsBKElement { get; set; }

        public string IsComplete { get; set; }
        

        public Element()
        {
            this.ParamList = new ObservableCollection<ElementParam>();
            this.ParamList.CollectionChanged += ParamList_CollectionChanged;
        }

        //Устанавливаем флажок наличия параметров, если изменилась коллекция ParamList
        void ParamList_CollectionChanged(object sender, global::System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ParamList != null && ParamList.Count > 0)
            {
                HasParams = true;
            }
        }
    }
}

