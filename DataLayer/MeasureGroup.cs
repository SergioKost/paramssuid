﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class MeasureGroup : Entity, ICloneable
    {
        public string Name { get; set; }

        public List<Measure> Measures { get; set; }

        public object Clone()
        {
            MeasureGroup resultCopy = new MeasureGroup();
            resultCopy.Id = this.Id;
            resultCopy.Name = this.Name;

            if (this.Measures != null)
            {
                resultCopy.Measures = new List<Measure>();
                foreach (Measure item in this.Measures)
                {
                    resultCopy.Measures.Add((Measure)item.Clone());
                }
            }



            return resultCopy;
        }
    }
}
