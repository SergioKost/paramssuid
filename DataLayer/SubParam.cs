﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class SubParam : Entity, ICloneable
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string Source { get; set; }

        public string Code { get; set; }

        public Guid ParameterId { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return String.Format("Name: {0}\r\n" +
                                 "Description: {1}\r\n" +
                                 "Source: {2}\r\n" +
                                 "Code: {3}\r\n", Name, Description, Source, Code);
        }
    }
}
