﻿//using Microsoft.VisualStudio.DebuggerVisualizers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;


namespace DataLayer
{
    public class Parametr : Entity, ICloneable
    {
        

        //Описание
        public string Description { get; set; }
        //Ед.изм.
        //public string Unit { get; set; }

        public ObservableCollection<SubParam> SubParameters { get; set; }

        public string Code { get; set; }


        //public Guid MeasureGroupId { get; set; }

        public MeasureGroup MeasureGroup { get; set; }

        //список возможных MeasureGroup
        public List<MeasureGroup> ListMeasureGroup { get; set; }


        public ParameterGroup ParameterGroup { get; set; }

        //список возможных ParameterGroup
        public List<ParameterGroup> ListParameterGroup { get; set; }


        public Parametr()
        {
            this.SubParameters = new ObservableCollection<SubParam>();
        }

        public override string ToString()
        {
            return String.Format("Description: {0}\r\n" +
                                 "Code: {1}\r\n" +                                
                                 "MeasureGroup: {2}\r\n" +
                                 "ParameterGroup: {3}\r\n", Description, Code, MeasureGroup.Name, ParameterGroup.Name);
        }

        public object Clone()
        {
            Parametr resultCopy = new Parametr();
            resultCopy.Id = this.Id;
            resultCopy.Description = this.Description;
            //resultCopy.Unit = this.Unit;
            resultCopy.Code = this.Code;

            if (this.MeasureGroup != null)
            {
                resultCopy.MeasureGroup = (MeasureGroup)this.MeasureGroup.Clone();
            }

            if (this.ListMeasureGroup != null)
            {
                resultCopy.ListMeasureGroup = new List<MeasureGroup>();
                foreach (MeasureGroup mg in this.ListMeasureGroup)
                {
                    resultCopy.ListMeasureGroup.Add((MeasureGroup)mg.Clone());
                }                
            }

            if (this.ParameterGroup != null)
            {
                resultCopy.ParameterGroup = (ParameterGroup)this.ParameterGroup.Clone();
            }

            if (this.ListParameterGroup != null)
            {
                resultCopy.ListParameterGroup = new List<ParameterGroup>();
                foreach (ParameterGroup pg in this.ListParameterGroup)
                {
                    resultCopy.ListParameterGroup.Add((ParameterGroup)pg.Clone());
                }
            }

            if (this.SubParameters != null)
            {
                resultCopy.SubParameters = new ObservableCollection<SubParam>();
                foreach (SubParam sParam in this.SubParameters)
                {
                    resultCopy.SubParameters.Add((SubParam)sParam.Clone());
                }
            }

            return resultCopy;
        }
    }
}

