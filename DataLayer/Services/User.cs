﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Services
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string FIO { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Office { get; set; }
        public bool IsAdmin { get; set; }
    }
}
