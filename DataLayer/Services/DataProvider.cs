﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Services
{
    public static class DataProvider
    {
        //Возвращает список проектов, для которых используется таблицы данных, как для БК
        public static List<string> GetFacilitiesCodesForBKTables()
        {
            return new List<string>() { "50", "60", "70" };


        }

        //Возвращает имя текущего пользователя (ВРЕМЕННО!)
        public static string GetUserName()
        {
            return Environment.UserDomainName + @"\" + Environment.UserName;

        }
    }
}
