﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Facility : Entity
    {
        //public int Id { get; set; }
        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Code { get; set; }

        public virtual ICollection<Element> Elements { get; set; }

        public virtual ICollection<Complex> Complexes { get; set; }
    }
}

