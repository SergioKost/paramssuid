﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Complex : Entity
    {
        //public int Id { get; set; }
        public string Name { get; set; }

        public string SppCode { get; set; }

        public string Code { get; set; }

        public virtual Facility Facility { get; set; }
        public virtual ICollection<System> Systems { get; set; }
    }
}

