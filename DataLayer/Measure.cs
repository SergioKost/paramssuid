﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Measure : Entity, ICloneable
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }

        public string Code { get; set; }
        public string Ratio { get; set; }
        
        public bool IsHead { get; set; }

        public Guid MeasureGroupId { get; set; }

        public MeasureGroup MeasureGroup { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
