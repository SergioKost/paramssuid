﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Stage : Entity, ICloneable
    {
        public string StageName  { get; set; }

        public string StageDescription { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
