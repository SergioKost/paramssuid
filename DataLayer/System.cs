﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class System : Entity
    {
        //public int Id { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Element> Elements { get; set; }
        public virtual Complex Complex { get; set; }
    }
}
