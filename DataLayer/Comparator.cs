﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Comparator<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {

            if (x is Parametr && y is Parametr)
            {
                return String.Compare((x as Parametr).Description, (y as Parametr).Description);
            }
            else
            {
                return String.Compare((x as Measure).Name, (y as Measure).Name);
            }
        }
    }
}
