﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ElementParam : Entity, ICloneable
    {
        public static object Error = "Введенные данные не прошли проверку";

        //Описание
        public string Description { get; set; }

        //Ед. измерения
        public string Unit { get; set; }
        //список возможных ед.изм
        public List<Measure> ListMeasure { get; set; }
        //Ед. изм.
        public Measure Measure { get; set; }

        public Stage Stage { get; set; }

        public List<Stage> ListStage { get; set; }

        //Значение
        public string Value { get; set; }
        //Лист значений, если параметр списочный
        public List<SubParam> ListValue { get; set; }

        public SubParam SubParamValue { get; set; }
        
        //Для вставки в БД
        //ИД параметра        
        public Guid ParameterId { get; set; }
        
        //ИД единицы измерения        
        //public Guid? MeasureId { get; set; }

        
        //Глубокое копирование ElementParam
        public object Clone()
        {
            ElementParam resultCopy = new ElementParam();
            //resultCopy.Id = this.Id;
            resultCopy.Description = this.Description;
            resultCopy.Unit = this.Unit;

            if (this.ListMeasure != null)
            {
                resultCopy.ListMeasure = new List<Measure>();
                foreach (Measure item in this.ListMeasure)
                {
                    resultCopy.ListMeasure.Add((Measure)item.Clone());
                }
            }

            
            if (this.Measure != null)
            {
                
                resultCopy.Measure = (Measure)this.Measure.Clone();
            }

            if (this.ListValue != null)
            {
                resultCopy.ListValue = new List<SubParam>();
                foreach (SubParam item in this.ListValue)
                {
                    resultCopy.ListValue.Add((SubParam)item.Clone());
                }
            }

            if (this.SubParamValue != null && resultCopy.ListValue != null)
            {
                //resultCopy.SubParamValue  = (SubParam)this.SubParamValue.Clone();
                resultCopy.SubParamValue = resultCopy.ListValue.FirstOrDefault(x => (x.Id == this.SubParamValue.Id));
                
            }

            if (this.Stage != null)
            {
                resultCopy.Stage = (Stage)this.Stage.Clone();
            }
            if (this.ListStage != null)
            {
                resultCopy.ListStage = new List<Stage>();
                foreach (Stage item in this.ListStage)
                {
                    resultCopy.ListStage.Add((Stage)item.Clone());
                }
            }

           
            resultCopy.Value = this.Value;
            resultCopy.ParameterId = this.ParameterId;
            //resultCopy.MeasureId = this.MeasureId;

            return resultCopy;
        }

        public static bool ValidateValue(string value)
     {
            if (value == null)
                value = "";

            Regex regex = new Regex(@"[0-9],[0-9]");
            return !regex.IsMatch(value);

            //Regex regex1 = new Regex(@"^\w");
            //if (regex1.IsMatch(value))
            //{
            //    return true;
            //}
            //else
            //{
            //    Regex regex = new Regex(@",");
            //    return !regex.IsMatch(value);

            //}

        }
    }
}
