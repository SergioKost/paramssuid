﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ParameterGroup : Entity, ICloneable
    {
        public string Name { get; set; }

        public string Code { get; set; }

        //public List<Parametr> Parameters { get; set; }

        public object Clone()
        {
            ParameterGroup resultCopy = new ParameterGroup();
            resultCopy.Id = this.Id;
            resultCopy.Name = this.Name;            
            resultCopy.Code = this.Code;                        

            return resultCopy;
        }
    }
}
