﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Rakushka.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Rakushka.Repository
{
    public class ParameterRepository : IParameterRepository
    {
        public IEnumerable<Parametr> SelectAll()
        {
            List<Parametr> result = new List<Parametr>();
            //try
            //{
            using (var context = new InfoModelRakushEntities())
            {
                List<Parameters> dbParCollection = context.Parameters.Where(x => (x.ParameterId != null)).ToList();
                foreach (Parameters item in dbParCollection)
                {
                    //item.SubParameters1 = context.SubParameters1.Where(y => (y.ParameterId == item.ParameterId)).ToList();
                    result.Add(Mapper.MapParameters(item));
                }
            }

            //}
            //catch (Exception er)
            //{
            //    MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            return result;
        }

        public Task<IEnumerable<Parametr>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public Parametr Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Parametr entity)
        {
            using (var context = new InfoModelRakushEntities())
            {
                var itemDb = context.Parameters.FirstOrDefault(x => x.ParameterId == entity.Id);
                if (itemDb == null)
                {
                    return false;
                }
                context.Entry(itemDb).CurrentValues.SetValues(Mapper.MapParameters(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Add(Parametr entity)
        {
            using (InfoModelRakushEntities context = new InfoModelRakushEntities())
            {
                context.Parameters.Add(Mapper.MapParameters(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Parametr param)
        {
            bool result = false;
            using (InfoModelRakushEntities context = new InfoModelRakushEntities())
            {

                //Проверка на используемое значение в таблице ElementsParameters
                int countRef = context.ElementsParameters.Where(x => (x.ParameterId == param.Id)).Count();

                //Проверка на используемое значение в таблице SubParameters
                int countRef2 = context.SubParameters.Where(z => (z.ParameterId == param.Id)).Count();

                if (countRef == 0 && countRef2 == 0)
                {
                    //Удаление
                    Parameters entity = context.Parameters.Where(y => (y.ParameterId == param.Id)).FirstOrDefault();
                    context.Parameters.Remove(entity);
                    context.SaveChanges();
                    result = true;
                }
            }
            return result;

        }

        public List<Parametr> SelectByCode(string parameterCode)
        {
            List<Parametr> result = new List<Parametr>();

            using (var context = new InfoModelRakushEntities())
            {
                List<Parameters> dbParamCollection = context.Parameters.Where(x => (x.ParameterCode == parameterCode)).ToList();
                foreach (Parameters item in dbParamCollection)
                {
                    result.Add(Mapper.MapParameters(item));
                }
            }

            return result;
        }
    }
}
