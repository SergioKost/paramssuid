﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Rakushka.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Rakushka.Repository
{
    public class SubParamRepository : ISubParamRepository
    {
        public bool Add(SubParam entity)
        {
            using (InfoModelRakushEntities context = new InfoModelRakushEntities())
            {
                context.SubParameters.Add(Mapper.MapSubParameter(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(SubParam sp)
        {
            bool result = false;
            using (InfoModelRakushEntities context = new InfoModelRakushEntities())
            {

                //Проверка на используемое значение
                int countRef = context.ElementsParameters.Where(x => (x.ParametrValue == sp.Id.ToString())).Count();
                if (countRef == 0)
                {
                    //Удаление
                    SubParameters entity = context.SubParameters.Where(y => (y.SubParameterId == sp.Id)).FirstOrDefault();                    
                    context.SubParameters.Remove(entity);
                    context.SaveChanges();
                    result = true;
                }               
            }
            return result;           

        }

        public SubParam Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubParam> SelectAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<SubParam>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public List<SubParam> SelectByCode(string subParameterCode)
        {
            List<SubParam> result = new List<SubParam>();

            using (var context = new InfoModelRakushEntities())
            {
                List<SubParameters> dbSubParCollection = context.SubParameters.Where(x => (x.SubParameterCode == subParameterCode)).ToList();
                foreach (SubParameters item in dbSubParCollection)
                {
                    result.Add(Mapper.MapSubParameter(item));
                }
            }

            return result;
        }

        public List<SubParam> SelectByCodeName(string subParameterCode, string subParameterName)
        {
            List<SubParam> result = new List<SubParam>();

            using (var context = new InfoModelRakushEntities())
            {
                List<SubParameters> dbSubParCollection = context.SubParameters.Where(x => (x.SubParameterCode == subParameterCode && x.SubParameterName == subParameterName)).ToList();
                foreach (SubParameters item in dbSubParCollection)
                {
                    result.Add(Mapper.MapSubParameter(item));
                }
            }

            return result;
        }

        public List<SubParam> SelectByParameterId(Guid parameterId)
        {
            List<SubParam> result = new List<SubParam>();

            using (var context = new InfoModelRakushEntities())
            {
                List<SubParameters> dbSubParCollection = context.SubParameters.Where(x => (x.ParameterId == parameterId)).ToList();
                foreach (SubParameters item in dbSubParCollection)
                {
                    result.Add(Mapper.MapSubParameter(item));
                }
            }

            return result;
        }

        public bool Update(SubParam entity)
        {
            using (var context = new InfoModelRakushEntities())
            {
                var itemDb = context.SubParameters.FirstOrDefault(x => x.SubParameterId == entity.Id);
                if (itemDb == null)
                {
                    return false;

                }
                context.Entry(itemDb).CurrentValues.SetValues(Mapper.MapSubParameter(entity));
                context.SaveChanges();
            }
            return true;
        }
    }
}
