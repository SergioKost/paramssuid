//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EF.Repository.Rakushka
{
    using System;
    using System.Collections.Generic;
    
    public partial class Measures
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Measures()
        {
            this.ElementsParameters = new HashSet<ElementsParameters>();
        }
    
        public System.Guid MeasureId { get; set; }
        public string MeasureName { get; set; }
        public Nullable<System.Guid> MeasureGroupId { get; set; }
        public string MeasureAbbreviation { get; set; }
        public string MeasureRatio { get; set; }
        public string MeasureCode { get; set; }
        public bool IsHeadMeasure { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElementsParameters> ElementsParameters { get; set; }
        public virtual MeasuresGroups MeasuresGroups { get; set; }
    }
}
