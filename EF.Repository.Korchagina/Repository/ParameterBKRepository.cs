﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Korchagina.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Korchagina.Repository
{
    public class ParameterBKRepository : IParameterRepository
    {
        public IEnumerable<Parametr> SelectAll()
        {
            List<Parametr> result = new List<Parametr>();
            //try
            //{
            using (var context = new InfoModelKorEntities())
            {
                List<Parameters1> dbParCollection = context.Parameters1.Where(x => (x.ParameterId != null)).ToList();
                foreach (Parameters1 item in dbParCollection)
                {
                    //item.SubParameters1 = context.SubParameters1.Where(y => (y.ParameterId == item.ParameterId)).ToList();
                    result.Add(Mapper.MapParameters1(item));
                }
            }

            //}
            //catch (Exception er)
            //{
            //    MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            return result;
        }

        public Task<IEnumerable<Parametr>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public Parametr Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Parametr entity)
        {
            using (var context = new InfoModelKorEntities())
            {
                var itemDb = context.Parameters1.FirstOrDefault(x => x.ParameterId == entity.Id);
                if (itemDb == null)
                {
                    return false;
                }
                context.Entry(itemDb).CurrentValues.SetValues(Mapper.MapParameters1(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Add(Parametr entity)
        {
            using (InfoModelKorEntities context = new InfoModelKorEntities())
            {
                context.Parameters1.Add(Mapper.MapParameters1(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Parametr param)
        {
            bool result = false;
            using (InfoModelKorEntities context = new InfoModelKorEntities())
            {

                //Проверка на используемое значение в таблице ElementsParameters
                int countRef = context.ElementsParametersForBK.Where(x => (x.ParameterId == param.Id)).Count();

                //Проверка на используемое значение в таблице SubParameters
                int countRef2 = context.SubParameters1.Where(z => (z.ParameterId == param.Id)).Count();

                if (countRef == 0 && countRef2 == 0)
                {
                    //Удаление
                    Parameters1 entity = context.Parameters1.Where(y => (y.ParameterId == param.Id)).FirstOrDefault();
                    context.Parameters1.Remove(entity);
                    context.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        public List<Parametr> SelectByCode(string parameterCode)
        {
            List<Parametr> result = new List<Parametr>();

            using (var context = new InfoModelKorEntities())
            {
                List<Parameters1> dbParamCollection = context.Parameters1.Where(x => (x.ParameterCode == parameterCode)).ToList();
                foreach (Parameters1 item in dbParamCollection)
                {
                    result.Add(Mapper.MapParameters1(item));
                }
            }

            return result;
        }
    }
}
