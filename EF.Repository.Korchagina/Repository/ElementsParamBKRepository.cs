﻿using DataLayer;
using DataLayer.Services;
using EF.Repository.Abstract.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Korchagina.Repository
{
    public class ElementsParamBKRepository : IElementParamRepository
    {
        public IEnumerable<ElementParam> SelectAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ElementParam>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public ElementParam Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(ElementParam entity)
        {
            try
            {
                using (InfoModelKorEntities context = new InfoModelKorEntities())
                {
                    var t = context.ElementsParametersForBK.FirstOrDefault(x => (x.ElementParameterId == entity.Id));
                    //t = Mapper.MapElementParam(entity);
                    if (t != null)
                    {
                        //Обновляем значения
                        if (entity.ListValue != null && entity.ListValue.Count > 0)
                        {
                            //Параметр списочный
                            t.ParametrValue = entity.SubParamValue.Id.ToString();
                        }
                        else
                        {
                            //Строковый параметр
                            t.ParametrValue = entity.Value;
                        }

                        //Обновляем ед.измерения
                        if (entity.Measure != null)
                        {
                            t.MeasureId = entity.Measure.Id;
                        }

                        //Обновляем стадию
                        if (entity.Stage != null)
                        {
                            t.StageId = entity.Stage.Id;
                        }

                        t.UserName = DataProvider.GetUserName();
                        t.ModifyDateTime = DateTime.Now;

                        context.SaveChanges();
                    }

                }
                return true;
            }
            catch
            {

                throw;
            }
        }


        //Для копирования параметров со значениями
        public bool AddCopy(ElementParam elParam, Element el)
        {
            try
            {
                using (InfoModelKorEntities context = new InfoModelKorEntities())
                {
                    ElementsParametersForBK dbElemParam = new ElementsParametersForBK();
                    dbElemParam.ElementParameterId = Guid.NewGuid();
                    dbElemParam.ElementId = el.Id;
                    dbElemParam.ParameterId = elParam.ParameterId;
                    dbElemParam.UserName = DataProvider.GetUserName();
                    dbElemParam.CreateDateTime = DateTime.Now;

                    if (elParam.Stage != null)
                    {
                        dbElemParam.StageId = elParam.Stage.Id;
                    }
                    //if (elParam.MeasureId != null)
                    //{
                    //    dbElemParam.MeasureId = elParam.MeasureId;
                    // }

                    if (elParam.Measure != null)
                    {
                        dbElemParam.MeasureId = elParam.Measure.Id;
                    }



                    //Копируем значения
                    //списочное
                    if (elParam.ListValue != null && elParam.ListValue.Count > 0 && elParam.SubParamValue != null)
                    {
                        //Параметр списочный
                        dbElemParam.ParametrValue = elParam.SubParamValue.Id.ToString();
                    }

                    //строковое
                    if (elParam.Value != null)
                    {
                        dbElemParam.ParametrValue = elParam.Value;
                    }
                    context.ElementsParametersForBK.Add(dbElemParam);
                    context.SaveChanges();
                }
                return true;
            }
            catch
            {

                throw;
            }

        }

        public bool Add(ElementParam ep, Parametr param, Measure meas, Element el)
        {
            try
            {
                using (InfoModelKorEntities context = new InfoModelKorEntities())
                {
                    ElementsParametersForBK dbElemParam = new ElementsParametersForBK();
                    dbElemParam.ElementParameterId = ep.Id;
                    dbElemParam.ElementId = el.Id;
                    dbElemParam.ParameterId = param.Id;
                    dbElemParam.StageId = ep.Stage.Id;
                    dbElemParam.UserName = DataProvider.GetUserName();
                    dbElemParam.CreateDateTime = DateTime.Now;

                    if (meas != null)
                    {
                        dbElemParam.MeasureId = meas.Id;
                    }
                    context.ElementsParametersForBK.Add(dbElemParam);
                    context.SaveChanges();

                }
                return true;
            }
            catch
            {
                throw;
            }

        }

        public bool Add(ElementParam elParam)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(ElementParam entity)
        {
            try
            {
                using (InfoModelKorEntities context = new InfoModelKorEntities())
                {
                    ElementsParametersForBK dbElParams = context.ElementsParametersForBK.Where(x => (x.ElementParameterId == entity.Id)).FirstOrDefault();
                    context.ElementsParametersForBK.Remove(dbElParams);
                    context.SaveChanges();
                    return true;
                }

            }
            catch
            {
                throw;
            }



        }
    }
}
