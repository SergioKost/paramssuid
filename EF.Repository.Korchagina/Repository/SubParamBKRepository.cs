﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Korchagina.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Korchagina.Repository
{
    public class SubParamBKRepository : ISubParamRepository
    {
        public bool Add(SubParam entity)
        {
            using (InfoModelKorEntities context = new InfoModelKorEntities())
            {
                context.SubParameters1.Add(Mapper.MapSubParameter(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(SubParam sp)
        {
            bool result = false;
            using (InfoModelKorEntities context = new InfoModelKorEntities())
            {

                //Проверка на используемое значение
                int countRef = context.ElementsParameters.Where(x => (x.ParametrValue == sp.Id.ToString())).Count();
                if (countRef == 0)
                {
                    //Удаление
                    SubParameters entity = context.SubParameters.Where(y => (y.SubParameterId == sp.Id)).FirstOrDefault();
                    context.SubParameters.Remove(entity);
                    context.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        public SubParam Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubParam> SelectAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<SubParam>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public List<SubParam> SelectByCode(string subParameterCode)
        {
            List<SubParam> result = new List<SubParam>();

            using (var context = new InfoModelKorEntities())
            {
                List<SubParameters1> dbSubParCollection = context.SubParameters1.Where(x => (x.SubParameterCode == subParameterCode)).ToList();
                foreach (SubParameters1 item in dbSubParCollection)
                {
                    result.Add(Mapper.MapSubParameter(item));
                }
            }

            return result;
        }

        public List<SubParam> SelectByCodeName(string subParameterCode, string subParameterName)
        {
            throw new NotImplementedException();
        }

        public List<SubParam> SelectByParameterId(Guid parameterId)
        {
            List<SubParam> result = new List<SubParam>();

            using (var context = new InfoModelKorEntities())
            {
                List<SubParameters1> dbSubParCollection = context.SubParameters1.Where(x => (x.ParameterId == parameterId)).ToList();
                foreach (SubParameters1 item in dbSubParCollection)
                {
                    result.Add(Mapper.MapSubParameter(item));
                }
            }

            return result;
        }

        public bool Update(SubParam entity)
        {
            using (var context = new InfoModelKorEntities())
            {
                var itemDb = context.SubParameters1.FirstOrDefault(x => x.SubParameterId == entity.Id);
                if (itemDb == null)
                {
                    return false;

                }
                context.Entry(itemDb).CurrentValues.SetValues(Mapper.MapSubParameter(entity));
                context.SaveChanges();
            }
            return true;
        }
    }
}
