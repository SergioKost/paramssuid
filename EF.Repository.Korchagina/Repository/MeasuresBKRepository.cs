﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Korchagina.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Korchagina.Repository
{
    public class MeasuresBKRepository : IMeasuresRepository
    {
        public IEnumerable<Measure> SelectAll()
        {
            List<Measure> result = new List<Measure>();
            
            using (var context = new InfoModelKorEntities())
            {
                List<Measures1> dbMeasCollection = context.Measures1.Where(x => (x.MeasureId != null)).ToList();
                foreach (Measures1 item in dbMeasCollection)
                {
                    result.Add(Mapper.MapMeasures(item));
                }
            }

           
            return result;
        }

        public Task<IEnumerable<Measure>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public Measure Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Measure entity)
        {
            throw new NotImplementedException();
        }

        public bool Add(Measure entity)
        {
            using (InfoModelKorEntities context = new InfoModelKorEntities())
            {
                context.Measures1.Add(Mapper.MapMeasures(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Measure entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Measure> SelectByParameterId(Guid parameterId)
        {
            List<Measure> result = new List<Measure>();

            using (var context = new InfoModelKorEntities())
            {

                //Получить MeasuresGroupId
                Guid? grId = context.Parameters1.Where(x => x.ParameterId == parameterId).FirstOrDefault().MeasureGroupId;
                
                if (grId != null)
                {
                    string measGrName = context.MeasuresGroups.Where(x => x.MeasureGroupId == grId).FirstOrDefault().MeasureGroupName;
                    if (measGrName == "ALL")
                    {
                        List<Measures1> dbMeasCollection = context.Measures1.Where(x => (x.MeasureId != null)).ToList();
                        foreach (Measures1 item in dbMeasCollection)
                        {
                            result.Add(Mapper.MapMeasures(item));
                        }
                    }
                    else
                    {

                        List<Measures1> dbMeasCollection = context.Measures1.Where(x => (x.MeasureGroupId == grId)).ToList();
                        foreach (Measures1 item in dbMeasCollection)
                        {
                            result.Add(Mapper.MapMeasures(item));
                        }
                    }

                }
            }

            return result;
        }
    }
}
