﻿using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Korchagina.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Korchagina.Repository
{
    public class ParameterRepository : IParameterRepository
    {
        public IEnumerable<Parametr> SelectAll()
        {
            List<Parametr> result = new List<Parametr>();
            //try
            //{
            using (var context = new InfoModelKorEntities())
            {
                List<Parameters> dbParCollection = context.Parameters.Where(x => (x.ParameterId != null)).ToList();
                foreach (Parameters item in dbParCollection)
                {
                    //item.SubParameters1 = context.SubParameters1.Where(y => (y.ParameterId == item.ParameterId)).ToList();
                    result.Add(Mapper.MapParameters(item));
                }
            }

            //}
            //catch (Exception er)
            //{
            //    MessageBox.Show(String.Format("Что-то пошло не так при загрузке Объектов из БД. Текст ошибки: {0}", er.ToString()), "Выбор объектов", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            return result;
        }

        public Task<IEnumerable<Parametr>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public Parametr Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Parametr entity)
        {
            throw new NotImplementedException();
        }

        public bool Add(Parametr entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Parametr entity)
        {
            throw new NotImplementedException();
        }

        public List<Parametr> SelectByCode(string parameterCode)
        {
            throw new NotImplementedException();
        }
    }
}
