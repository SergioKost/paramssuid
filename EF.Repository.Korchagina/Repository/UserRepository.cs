﻿using DataLayer.Services;
using EF.Repository.Abstract.Common;
using EF.Repository.Korchagina.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Korchagina.Repository
{
    public class UserRepository : IUserRepository
    {
        public bool Add(User entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public User Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> SelectAll()
        {
            List<User> result = new List<User>();
            using (var context = new InfoModelKorEntities())
            {
                List<Users> dbUserCollection = context.Users.Where(x => (x.Id != null)).ToList();
                foreach (Users item in dbUserCollection)
                {
                    result.Add(Mapper.MapUser(item));
                }
            }
            return result;
        }

        public Task<IEnumerable<User>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public List<User> SelectByName(string name)
        {
            List<User> result = new List<User>();
            using (var context = new InfoModelKorEntities())
            {
                List<Users> dbUserCollection = context.Users.Where(x => (x.UserPrincipalName.ToLower() == name.ToLower())).ToList();
                foreach (Users item in dbUserCollection)
                {
                    result.Add(Mapper.MapUser(item));
                }
            }
            return result;
        }

        public bool Update(User entity)
        {
            throw new NotImplementedException();
        }
    }
}
