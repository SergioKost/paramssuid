﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EF.Repository.Abstract.Common;
using EF.Repository.Korchagina.Mappers;

namespace EF.Repository.Korchagina.Repository
{
    public class MeasureGroupsRepository : IMeasureGroupsRepository
    {
        public bool Add(MeasureGroup entity)
        {
            using (InfoModelKorEntities context = new InfoModelKorEntities())
            {
                context.MeasuresGroups.Add(Mapper.MapMeasureGroups(entity));
                context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(MeasureGroup entity)
        {
            throw new NotImplementedException();
        }

        public MeasureGroup Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MeasureGroup> SelectAll()
        {
            List<MeasureGroup> result = new List<MeasureGroup>();
            
            using (var context = new InfoModelKorEntities())
            {
                List<MeasuresGroups> dbMeasCollection = context.MeasuresGroups.Where(x => (x.MeasureGroupId != null)).ToList();
                foreach (MeasuresGroups item in dbMeasCollection)
                {
                    result.Add(Mapper.MapMeasureGroups(item));
                }
            }

            return result;
        }

        public Task<IEnumerable<MeasureGroup>> SelectAllAsync()
        {
            throw new NotImplementedException();
        }

        public bool Update(MeasureGroup entity)
        {
            throw new NotImplementedException();
        }
    }
}
