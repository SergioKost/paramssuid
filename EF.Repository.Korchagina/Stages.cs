//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EF.Repository.Korchagina
{
    using System;
    using System.Collections.Generic;
    
    public partial class Stages
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Stages()
        {
            this.ElementsParameters = new HashSet<ElementsParameters>();
            this.ElementsParametersForBK = new HashSet<ElementsParametersForBK>();
        }
    
        public System.Guid StageId { get; set; }
        public string StageName { get; set; }
        public string StageDescription { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElementsParameters> ElementsParameters { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElementsParametersForBK> ElementsParametersForBK { get; set; }
    }
}
