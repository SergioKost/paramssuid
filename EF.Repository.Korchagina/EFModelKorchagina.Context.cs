﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EF.Repository.Korchagina
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class InfoModelKorEntities : DbContext
    {
        public InfoModelKorEntities()
            : base("name=InfoModelKorEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Facilities> Facilities { get; set; }
        public virtual DbSet<ElementsParameters> ElementsParameters { get; set; }
        public virtual DbSet<Complexes> Complexes { get; set; }
        public virtual DbSet<Systems> Systems { get; set; }
        public virtual DbSet<Elements> Elements { get; set; }
        public virtual DbSet<Parameters> Parameters { get; set; }
        public virtual DbSet<SubParameters> SubParameters { get; set; }
        public virtual DbSet<Measures> Measures { get; set; }
        public virtual DbSet<ElementTypes> ElementTypes { get; set; }
        public virtual DbSet<Measures1> Measures1 { get; set; }
        public virtual DbSet<Parameters1> Parameters1 { get; set; }
        public virtual DbSet<SubParameters1> SubParameters1 { get; set; }
        public virtual DbSet<ElementsParametersForBK> ElementsParametersForBK { get; set; }
        public virtual DbSet<Stages> Stages { get; set; }
        public virtual DbSet<MeasuresGroups> MeasuresGroups { get; set; }
        public virtual DbSet<ParametersGroups> ParametersGroups { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UserParamAdmin> UserParamAdmin { get; set; }
    }
}
