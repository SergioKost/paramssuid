﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface IElementRepository : IRepository<Element>
    {
        bool SetIsComplete(Element entity, string isComplete);
        IEnumerable<Element> SelectBySystem(Guid systemId);
    }
}
