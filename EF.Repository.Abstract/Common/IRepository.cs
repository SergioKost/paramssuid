﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> SelectAll();

        Task<IEnumerable<T>> SelectAllAsync();

        T Select(Guid id);

        //bool Save(T entity);

        bool Update(T entity);

        bool Add(T entity);

        bool Delete(int id);
        bool Delete(T entity);
    }
}
