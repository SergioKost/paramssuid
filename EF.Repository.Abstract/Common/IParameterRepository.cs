﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface IParameterRepository : IRepository<Parametr>
    {
        List<Parametr> SelectByCode(string parameterCode);
    }
}
