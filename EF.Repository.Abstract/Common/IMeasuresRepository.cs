﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface IMeasuresRepository : IRepository<Measure>
    {
        /// <summary>
        /// формирует список Measures для данного Parameter
        /// </summary>
        /// <param name="parameterId"></param>
        /// <returns></returns>
        IEnumerable<Measure> SelectByParameterId(Guid parameterId);
    }
}
