﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface IElementParamRepository : IRepository<ElementParam>
    {
        bool AddCopy(ElementParam elParam, Element el);
        bool Add(ElementParam ep, Parametr param, Measure meas, Element el);
    }
}
