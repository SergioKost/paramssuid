﻿using DataLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface IUserRepository : IRepository<User>
    {
        List<User> SelectByName(string name);
    }
}
