﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Repository.Abstract.Common
{
    public interface ISubParamRepository : IRepository<SubParam>
    {
        List<SubParam> SelectByParameterId(Guid parameterId);

        List<SubParam> SelectByCode(string subParameterCode);
        List<SubParam> SelectByCodeName(string subParameterCode, string subParameterName);
    }
}
