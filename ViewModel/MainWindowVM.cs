﻿using System;
using System.Collections;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.TeamFoundation.MVVM;
using EF.Repository.Repository;
using EF.Repository;
using DataLayer;
using EF.Repository.Abstract;
using ViewModel.Services;
using GalaSoft.MvvmLight.Messaging;


//using GalaSoft.MvvmLight.Command;

namespace ViewModel
{
    public class MainWindowVM : ViewModelBase
    {

        RepositoryContainer rc;


        private ViewModelBase _selectedViewModel;
        private string _titleText;

        public ViewModelBase SelectedViewModel
        {
            get => _selectedViewModel;
            set
            {
                _selectedViewModel = value;
                RaisePropertyChanged("SelectedViewModel");
                CurrentUserIsAdmin = UserService.CheckAdmin(rc._userRep);

            }
        }
        public string TitleText
        {
            get => _titleText;
            set
            {
                _titleText = value;
                RaisePropertyChanged("TitleText");
            }
        }

        public string TitleProject { get; set; }
        
        //Команды
        public RelayCommand SelectFilanovkaCommand { get; set; }
        public RelayCommand SelectKorchaginaCommand { get; set; }
        public RelayCommand SelectRakushkaCommand { get; set; }

        //Справочники
        public RelayCommand ShowSubParameters { get; set; }
        public RelayCommand ShowParameters { get; set; }
        public RelayCommand ShowMeasures { get; set; }
        public RelayCommand ShowMeasureGroups { get; set; }

        public RelayCommand ShowListUserAdmin { get; set; }


        public RelayCommand ShowUserSettingWindow { get; set; }

        
        public bool CurrentUserIsAdmin { get; set; } 
            
      

        public MainWindowVM(GalaSoft.MvvmLight.Views.IDialogService dialogService)
        {
            TitleText = "Проект не выбран";

             SelectFilanovkaCommand = new RelayCommand((object x) =>
            {
                rc = new RepositoryContainer(0);
                SelectedViewModel = new ManageParamVM(dialogService, rc);
                TitleText = "Филановка";
                TitleProject = "Филановка";
                return;
            }
             );
            

            SelectKorchaginaCommand = new RelayCommand((object x) =>
            {

                rc = new RepositoryContainer(1);
                SelectedViewModel = new ManageParamVM(dialogService, rc);

                TitleText = "Корчагина";
                TitleProject = "Корчагина";
                return;
            });

            SelectRakushkaCommand = new RelayCommand((object x) =>
            {

                rc = new RepositoryContainer(2);
                SelectedViewModel = new ManageParamVM(dialogService, rc);
                TitleText = "Грайфера";
                TitleProject = "Грайфера";
                return;
            });


            ShowSubParameters = new RelayCommand((object x) =>
            {
                SelectedViewModel = new ManageSubParametersVM(dialogService, rc);
                TitleText = "Редактирование SubParameters: " + TitleProject;
                return;
            }, (object t) => { return CurrentUserIsAdmin && rc != null; });


            ShowParameters = new RelayCommand((object x) =>
            {
                SelectedViewModel = new ManageParametersVM(dialogService, rc);
                TitleText = "Редактирование Parameters: " + TitleProject;
                return;
            }, (object t) => { return CurrentUserIsAdmin && rc != null; });

            ShowMeasures = new RelayCommand((object x) =>
            {

                SelectedViewModel = new ManageMeasuresVM(dialogService, rc);


                TitleText = "Редактирование Measures: " + TitleProject;

                return;

            }, (object t) => { return CurrentUserIsAdmin && rc != null; });



            ShowMeasureGroups = new RelayCommand((object x) =>
            {

                SelectedViewModel = new ManageMeasureGroupsVM(dialogService, rc);


                TitleText = "Редактирование MeasureGroups: " + TitleProject;

                return;

            }, (object t) => { return CurrentUserIsAdmin && rc != null; });

            ShowUserSettingWindow = new RelayCommand(ShowSettingWindow, (object t) => { return true; });

            ShowListUserAdmin = new RelayCommand((object x) =>
            {
                SelectedViewModel = new ListUserAdminVM(dialogService, rc);
                TitleText = "Администраторы по проекту: " + TitleProject;

            }, (object t) => { return CurrentUserIsAdmin && rc != null; });

    }

       

        private void ShowSettingWindow(object obj)
        {
            //Запуск формы для редактирования пользовательских настроек          

            UserSettingsVM usVM = new UserSettingsVM();

            Messenger.Default.Send(usVM, "ShowSettingsWindow");
        }
    }
}
