﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.TeamFoundation.MVVM;
using EF.Repository.Repository;
using DataLayer;
using EF.Repository.Abstract.Common;
using System.Reflection;

namespace ViewModel
{
    //public class AddWindowVM : ViewModelBase
    public class AddWindowVM : ViewModelBase
    {
        public string TitleForm { get; set; }

        

        public Element ElementCur { get; set; }
        public List<Parametr> ParamList { get; set; }
        public List<Measure> MeasureList { get; set; }
        public List<Stage> StagesList {
            get;
            set; }
        private Measure _selectedMeasure;
        private readonly IElementParamRepository _rep;
        private readonly IStagesRepository _stRep;
        private readonly IMeasuresRepository repMeas;

        public Measure SelectedMeasure
        {
            get
            {
                return _selectedMeasure;
            }
            set
            {
                _selectedMeasure = value;
                RaisePropertyChanged("SelectedMeasure");
            }
        }


        private Parametr _selectedParametr;
        public Parametr SelectedParametr
        {
            get
            {
                return _selectedParametr;
            }
            set
            {
                _selectedParametr = value;
                RaisePropertyChanged("SelectedParametr");

                //MeasureList = MeasureList.Where(x => x.MeasureGroupId == value.MeasureGroupId).ToList();
                //MeasureList = repMeas.SelectAll()
                //    .Where(x => x.MeasureGroupId == value.MeasureGroupId)
                //    .Where(x => x.MeasureGroupId == value.MeasureGroup.Id)
                //    .ToList();

                MeasureList = repMeas.SelectByParameterId(value.Id).OrderBy(x => x.Name)                    
                    .ToList();

                RaisePropertyChanged("MeasureList");

                if (MeasureList != null && MeasureList.Count > 0)
                {
                    var headerMeasure = MeasureList.Where(x => x.IsHead).ToList();
                    if (headerMeasure.Count > 0)
                    {
                        SelectedMeasure = headerMeasure.First();
                    }
                    else
                    {
                        SelectedMeasure = MeasureList.First();
                    }
                    
                }

            }

        }
        public Stage SelectedStage { get; set; }

        public ICommand AddCommand { get; set; }

       

        public AddWindowVM(Element elem, List<Parametr> paramList, IElementParamRepository rep, IStagesRepository stRep, IMeasuresRepository repMeas)
        {
            ElementCur = elem;
            ParamList = paramList;
            //MeasureList = measureList;
            this._rep = rep;
            _stRep = stRep;
            this.repMeas = repMeas;
            StagesList = _stRep.SelectAll().ToList();
            AddCommand = new RelayCommand(SaveAdd, CanSaveAdd);
            
        }

        public void SetSelectedStage(string defaultStageName)
        {
            SelectedStage = StagesList.Where(x => x.StageName == defaultStageName).FirstOrDefault();
        }

        private bool CanSaveAdd(object obj)
        {
            return SelectedParametr != null && SelectedStage != null && SelectedMeasure != null;
        }

        public void SaveAdd(object obj)
        {
            ElementParam ep = new ElementParam();
            ep.Id = Guid.NewGuid();
            ep.Description = SelectedParametr.Description;
            ep.ListValue = SelectedParametr.SubParameters.ToList();
            ep.Stage = SelectedStage;
            if (SelectedMeasure != null)
            {
                ep.Unit = SelectedMeasure.Abbreviation;
                ep.Measure = SelectedMeasure;
               
            }
            ElementCur.ParamList.Add(ep);

            _rep.Add(ep, SelectedParametr, SelectedMeasure, ElementCur);
            
            
            //После добавления сбрасываем выбранную ед. изм.
            SelectedMeasure = null;

        }
    }
}
