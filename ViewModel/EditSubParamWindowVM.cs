﻿using DataLayer;
using DevExpress.Mvvm;
using EF.Repository.Abstract.Common;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Services;
using ViewModelBase = Microsoft.TeamFoundation.MVVM.ViewModelBase;

namespace ViewModel
{
    public class EditSubParamWindowVM : ViewModelBase, IDataErrorInfo
    {
        private SubParam editableSubParam;
        private readonly Parametr selectedParam;
        private readonly RepositoryContainer _rc;
        private ISubParamRepository _rep;        

        public string TitleForm { get; set; }
        public string ButtonText { get; set; }

        public string SubParameterName { get; set; }
        public string SubParameterCode { get; set; }
        public string SubParameterSource { get; set; }
        public string SubParameterDescription { get; set; }


        public System.Windows.Input.ICommand AddCommand { get; set; }

        public string Error => throw new NotImplementedException();
        public string this[string columnName]
        {
            get
            {
                string subParameterCodeProp = BindableBase.GetPropertyName(() => SubParameterCode);
                string subParameterNameProp = BindableBase.GetPropertyName(() => SubParameterName);
                if ((columnName == subParameterCodeProp && SubParameterCode != editableSubParam.Code) ||
                    (columnName == subParameterNameProp && SubParameterName != editableSubParam.Name))
                    if (Utilities.CheckCode(SubParameterCode, SubParameterName, selectedParam, _rc))
                        return "Код дублируется в базе!";
                return null;
            }
        }
        


        public EditSubParamWindowVM(SubParam editableSubParam, Parametr selectedParam,  RepositoryContainer rc)
        {
            this.editableSubParam = editableSubParam;
            this.selectedParam = selectedParam;
            this._rc = rc;
            _rep = rc._subParamRepBK;

            SubParameterName = editableSubParam.Name;
            SubParameterCode = editableSubParam.Code;
            SubParameterSource = editableSubParam.Source;
            SubParameterDescription = editableSubParam.Description;

            //AddCommand = new RelayCommand(UpdateSubParam, CanUpdate);
            AddCommand = new RelayCommand(UpdateSubParam);

            TitleForm = "Редатирование SubParameter" ;
            ButtonText = "Обновить";

        }


        private bool CanUpdate(object obj)
        {
            return SubParameterName != null
                && SubParameterCode != null
                //&& SubParameterSource !=null
                //&& SubParameterDescription !=null
                ;
        }



        private async void UpdateSubParam(object obj)
        {

            SubParam sp = new SubParam();
            sp.Id = editableSubParam.Id;
            sp.ParameterId = editableSubParam.ParameterId;
            sp.Name = SubParameterName;
            sp.Code = SubParameterCode;
            sp.Source = SubParameterSource;
            sp.Description = SubParameterDescription;

            _rep.Update(sp);

            
            ////Лютая дичь, для того, чтобы синхронизировать 2 БД - Зачем???
            //EF.Repository.Repository.SubParamBKRepository rep1 = new EF.Repository.Repository.SubParamBKRepository();
            //EF.Repository.Korchagina.Repository.SubParamBKRepository rep2 = new EF.Repository.Korchagina.Repository.SubParamBKRepository();
            //EF.Repository.Rakushka.Repository.SubParamRepository rep3 = new EF.Repository.Rakushka.Repository.SubParamRepository();


            //rep1.Update(sp);
            //rep2.Update(sp);
            //rep3.Update(sp);

            //Для формы
            var item = selectedParam.SubParameters.FirstOrDefault(x => x.Id == sp.Id);
            int i = selectedParam.SubParameters.IndexOf(item);
            selectedParam.SubParameters[i] = sp;

            //Уведомление
            string project = _rc.ProjectSelectedName;
            await Task.Run(() => EmailSender.SendParamInfo(String.Format("Проект: {0}\r\nПользователь изменил SubParameter ({1}).\r\n" +
                "Старое значение:\r\n{2}\r\nНовое значение:\r\n{3}",project, selectedParam.Description, editableSubParam.ToString(), sp.ToString())));
        }
    }
}
