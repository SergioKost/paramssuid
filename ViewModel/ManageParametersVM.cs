﻿using DataLayer;
using EF.Repository.Abstract.Common;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.TeamFoundation.MVVM;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ViewModel.Services;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModel
{
    public class ManageParametersVM : ViewModelBase
    {
        private GalaSoft.MvvmLight.Views.IDialogService _dialogService;

        private RepositoryContainer _rc;
        private IParameterRepository _rep;

        public Parametr SelectedParam { get; set; }


        private ObservableCollection<Parametr> _paramList;

        public ObservableCollection<Parametr> ParametersList
        {
            get { return _paramList; }
            set
            {
                _paramList = value;
                RaisePropertyChanged("ParametersList");

            }
        }

        //Команды
        public ICommand AddCommand { get; private set; }
        public ICommand EditCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public ManageParametersVM(GalaSoft.MvvmLight.Views.IDialogService dialogService, RepositoryContainer rc)
        {
            _rc = rc;
            _rep = rc._paramRepBK;
            _dialogService = dialogService;
            ParametersList = new ObservableCollection<Parametr>();
            foreach (Parametr item in rc._paramRepBK.SelectAll())
            {
                ParametersList.Add(item);
            }               

            AddCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(ShowAddParameter, CanAdd);
            EditCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(EditParameter, CanEdit);
            DeleteCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(DeleteParameter, CanDelete);

        }

        private void DeleteParameter(object obj)
        {
            string text = $"Вы действительно хотите удалить параметр \"{SelectedParam.Description}\"?";
            _dialogService.ShowMessage(text, "Confirm", "Да", "Нет", async (confirmed) => {
                if (confirmed)
                {
                    //Из БД
                    bool result = _rep.Delete(SelectedParam);
                    //Из формы
                    if (result)
                    {
                        //Уведомление
                        string project = _rc.ProjectSelectedName;
                        await Task.Run(() => EmailSender.SendParamInfo(String.Format("Проект: {0}\r\nПользователь удалил Parameter:\r\n{1}",project, SelectedParam.ToString())));
                        
                        Logger.Logger.Info($"Parameter \"{SelectedParam.Description}\" successfully deleted");
                        ParametersList.Remove(SelectedParam);                        
                        _dialogService.ShowMessageBox("Parameter удален", "Информация");
                        
                    }
                    else
                    {
                        _dialogService.ShowMessageBox("Невозможно удалить", "Информация");
                    }

                }

            });
        }

        private bool CanDelete(object obj)
        {
            return SelectedParam != null;
        }

        private bool CanEdit(object obj)
        {
            return SelectedParam != null;
        }

        private void EditParameter(object obj)
        {
            Parametr editParam = new Parametr();
            editParam = (Parametr)SelectedParam.Clone();
            editParam.ListParameterGroup = _rc._paramGrRep.SelectAll().ToList();
            editParam.ListMeasureGroup = _rc._mesGrRep.SelectAll().ToList();

            EditParameterWindowVM editVM = new EditParameterWindowVM(editParam, _rc, ParametersList);
            //Показ формы редактирования 
            Messenger.Default.Send(editVM, "ShowEditParamView");

        }

        private bool CanAdd(object obj)
        {
            return true;
        }

        private void ShowAddParameter(object obj)
        {
            AddParameterWindowVM addVM = new AddParameterWindowVM(_rc, ParametersList);
            //Показ формы добавления 
            Messenger.Default.Send(addVM, "ShowAddParameterView");
        }
    }
}
