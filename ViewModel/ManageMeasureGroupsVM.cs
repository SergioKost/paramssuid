﻿using DataLayer;
using EF.Repository.Abstract.Common;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;

namespace ViewModel
{
    public class ManageMeasureGroupsVM : ViewModelBase
    {
        
        private IMeasureGroupsRepository _repMeasGr;

        private List<MeasureGroup> _measGrList;

        //Команды
        public ICommand AddCommand { get; private set; }

        public List<MeasureGroup> MeasureGroupsList
        {
            get { return _measGrList; }
            set
            {
                _measGrList = value;
                RaisePropertyChanged("MeasureGroupsList");
            }
        }
      
        public ManageMeasureGroupsVM(GalaSoft.MvvmLight.Views.IDialogService dialogService, RepositoryContainer rc)
        {           
            this._repMeasGr = rc._mesGrRep;
            MeasureGroupsList = _repMeasGr.SelectAll().ToList();
            AddCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(ShowAddForm, CanAdd);
        }

        private bool CanAdd(object obj)
        {
            return true;
        }

        private void ShowAddForm(object obj)
        {
            AddMeasureGroupWindowVM addVM = new AddMeasureGroupWindowVM(_repMeasGr);
            //Показ формы добавления 
            Messenger.Default.Send(addVM, "AddMeasureGroupView");
        }
    }
}
