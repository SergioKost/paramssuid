﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ViewModel.Logger
{

    public static class Logger
    {
        public static void Error(Exception ex, string message)
        {
            NLog.Logger logger = LogManager.GetCurrentClassLogger();
            logger.Error(message);
            logger.Error(String.Format("ex.Message: {0}",ex.Message));
            logger.Error(String.Format("ex.InnerException: {0}",ex.InnerException));
            logger.Error(String.Format("ex.Source: {0}",ex.Source));
            logger.Error(String.Format("ex.Type: {0}", ex.GetType()));
            logger.Error(String.Format("ex.TargetSite: {0}", ex.TargetSite));
            logger.Error(String.Format("ex.StackTrace: {0}", ex.StackTrace));

        }

        public static void Info(string message)
        {
            NLog.Logger logger = LogManager.GetCurrentClassLogger();
            logger.Info(message);
        }


        
    }
}
