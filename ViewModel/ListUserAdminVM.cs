﻿using DataLayer;
using GalaSoft.MvvmLight.Messaging;


using EF.Repository.Abstract.Common;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;
using Microsoft.TeamFoundation.MVVM;
using DataLayer.Services;

namespace ViewModel
{
    public class ListUserAdminVM : ViewModelBase
    {
        private IUserRepository _repUser;

        private List<User> _userAdminList;

        

        public List<User> UserAdminList
        {
            get { return _userAdminList; }
            set
            {
                _userAdminList = value;
                RaisePropertyChanged("UserAdminList");
            }
        }
        public ListUserAdminVM(GalaSoft.MvvmLight.Views.IDialogService dialogService, RepositoryContainer rc)
        {
            this._repUser = rc._userRep;
            UserAdminList = _repUser.SelectAll().Where(x => x.IsAdmin).ToList();


        }
    }
}
