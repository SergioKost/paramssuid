﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public interface IWindowService
    {
        //void CreateAddEditWindow<T>(T viewModel);

        void CreateEditWindow(EditWindowVM viewModel);
        
        void CreateAddParamWindow(AddWindowVM viewModel);

        string CreateIsCompleteDialogWindow();

        bool ShowDialogWindow(string text);

        void ShowMessageWindow(string text);


    }
}
