﻿using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModel
{
    public class UserSettingsVM : ViewModelBase
    {
        public Action CloseAction { get; set; }
        public Action<string> SavePropStageAction { get; set; }

        public ICommand SaveCommand { get; set; }

        public string StageName { get; set; }

        public UserSettingsVM()
        {            
            SaveCommand = new RelayCommand(Save, CanSave);
        }

        private void Save(object obj)
        {
            SavePropStageAction(StageName);
            CloseAction();
        }

        private bool CanSave(object obj)
        {
            return true;
        }

        
    }
}
