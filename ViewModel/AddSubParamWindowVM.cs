﻿using DataLayer;
using DevExpress.Mvvm;
using EF.Repository.Abstract.Common;

using Microsoft.TeamFoundation.MVVM;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;
using ViewModelBase = Microsoft.TeamFoundation.MVVM.ViewModelBase;

namespace ViewModel
{
    public class AddSubParamWindowVM : ViewModelBase, IDataErrorInfo
    {
        private Parametr selectedParam;
        private readonly RepositoryContainer _rc;
        private readonly ISubParamRepository _rep;        

        public string TitleForm { get; set; }
        public string ButtonText { get; set; }

        public string SubParameterName { get; set; }
        public string SubParameterCode { get; set; }
        public string SubParameterSource { get; set; }
        public string SubParameterDescription { get; set; }


        public ICommand AddCommand { get; set; }

        
        public string Error => throw new NotImplementedException();
        public string this[string columnName] {
            get {
                string subParameterCodeProp = BindableBase.GetPropertyName(() => SubParameterCode);
                string subParameterNameProp = BindableBase.GetPropertyName(() => SubParameterName);
                if (columnName == subParameterCodeProp || columnName == subParameterNameProp)
                    if (Utilities.CheckCode(SubParameterCode, SubParameterName, selectedParam, _rc))
                        return "Код дублируется в базе!";
                return null;
            }
        }

        private bool CheckCode(string subParameterCode)
        {
            //Проверка на дублирование;
            return _rep.SelectByCode(subParameterCode).Count > 0;

        }

        public AddSubParamWindowVM(Parametr selectedParam, RepositoryContainer rc)
        {
            this.selectedParam = selectedParam;
            this._rc = rc;
            AddCommand = new RelayCommand(AddSubParam, CanAdd);
            //AddCommand = new RelayCommand(AddSubParam);
            
            this._rep = rc._subParamRepBK;
            TitleForm = "Добавление нового SubParameter";
            ButtonText = "Добавить";

        }

        private bool CanAdd(object obj)
        {
            return SubParameterName !=null 
                && SubParameterCode !=null
                //&& SubParameterSource !=null
                //&& SubParameterDescription !=null
                ;
        }

        

        private async void AddSubParam(object obj)
        {

            SubParam sp = new SubParam();
            sp.Id = Guid.NewGuid();
            sp.ParameterId = selectedParam.Id;
            sp.Name = SubParameterName;
            sp.Code = SubParameterCode;
            sp.Source = SubParameterSource;
            sp.Description = SubParameterDescription;

            _rep.Add(sp);

            ////Лютая дичь, для того, чтобы синхронизировать 2 БД - Зачем???
            //EF.Repository.Repository.SubParamBKRepository rep1 = new EF.Repository.Repository.SubParamBKRepository();
            //EF.Repository.Korchagina.Repository.SubParamBKRepository rep2 = new EF.Repository.Korchagina.Repository.SubParamBKRepository();
            //EF.Repository.Rakushka.Repository.SubParamRepository rep3 = new EF.Repository.Rakushka.Repository.SubParamRepository();

            //rep1.Add(sp);
            //rep2.Add(sp);
            //rep3.Add(sp);

            //Для формы
            selectedParam.SubParameters.Add(sp);


            //Уведомление
            string project = _rc.ProjectSelectedName;
            await Task.Run(() => EmailSender.SendParamInfo(String.Format("Проект: {0}\r\nПользователь добавил новый SubParameter ({1}).\r\n{2}", project, selectedParam.Description, sp.ToString())));



        }







        
    }
}
