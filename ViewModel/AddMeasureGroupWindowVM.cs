﻿using DataLayer;
using EF.Repository.Abstract.Common;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModel
{
    public class AddMeasureGroupWindowVM : ViewModelBase
    {
        private IMeasureGroupsRepository _repMeasGr;

        public string MeasureGroupName { get; set; }

        public ICommand AddCommand { get; set; }

        public AddMeasureGroupWindowVM(IMeasureGroupsRepository repMeasGr)
        {
            _repMeasGr = repMeasGr;
            AddCommand = new RelayCommand(AddMeasGroup, CanAdd);
        }

        private bool CanAdd(object obj)
        {
            return MeasureGroupName != null && MeasureGroupName != "";                
        }

        private void AddMeasGroup(object obj)
        {
            MeasureGroup mg = new MeasureGroup();
            mg.Id = Guid.NewGuid();
            mg.Name = MeasureGroupName;

            _repMeasGr.Add(mg); //Save to DB

            ////Лютая дичь, для того, чтобы синхронизировать 2 БД - Зачем???
            //EF.Repository.Repository.MeasureGroupsRepository rep1 = new EF.Repository.Repository.MeasureGroupsRepository();
            //EF.Repository.Korchagina.Repository.MeasureGroupsRepository rep2 = new EF.Repository.Korchagina.Repository.MeasureGroupsRepository();
            //EF.Repository.Rakushka.Repository.MeasureGroupsRepository rep3 = new EF.Repository.Rakushka.Repository.MeasureGroupsRepository();


            //rep1.Add(mg);
            //rep2.Add(mg);
            //rep3.Add(mg);


        }
    }
}
