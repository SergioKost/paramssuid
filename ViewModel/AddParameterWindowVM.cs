﻿using DataLayer;
using DevExpress.Mvvm;
using EF.Repository.Abstract.Common;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;
using ViewModelBase = Microsoft.TeamFoundation.MVVM.ViewModelBase;

namespace ViewModel
{
    public class AddParameterWindowVM : ViewModelBase, IDataErrorInfo
    {
        private IParameterRepository _paramRepBK;
        private IParametersGroupsRepository _paramGrRep;
        private IMeasureGroupsRepository _mesGrRep;
        private readonly RepositoryContainer _rc;
        private readonly ObservableCollection<Parametr> parametersList;

        public string TitleForm { get; set; }
        public string ButtonText { get; set; }


        public List<MeasureGroup> ListMeasureGroups { get; }
        public MeasureGroup SelectedMeasureGroup { get; set; }

        public List<ParameterGroup> ListParameterGroups { get; }

        public ParameterGroup SelectedParameterGroups { get; set; }

        public string ParameterName { get; set; }

        public string ParameterCode { get; set; }

        public ICommand AddCommand { get; set; }
        public bool EnableChangeMeasureGroup { get; }

        public string Error => throw new NotImplementedException();

        public string this[string columnName]
        {
            get
            {
                string parameterCodeProp = BindableBase.GetPropertyName(() => ParameterCode);
                if (columnName == parameterCodeProp)
                    if (CheckCode(ParameterCode))
                        return "Код дублируется в базе!";
                return null;
            }
        }

        private bool CheckCode(string parameterCode)
        {
            //Проверка на дублирование;
            return _paramRepBK.SelectByCode(parameterCode).Count > 0;
        }

        public AddParameterWindowVM(RepositoryContainer rc, ObservableCollection<Parametr> parametersList)
        {
            _paramRepBK = rc._paramRepBK;
            _paramGrRep = rc._paramGrRep;
            _mesGrRep = rc._mesGrRep;
            _rc = rc;

            this.parametersList = parametersList;
            ListMeasureGroups = _mesGrRep.SelectAll().ToList();
            ListParameterGroups = _paramGrRep.SelectAll().ToList();

            ButtonText = "Добавить параметр";
            TitleForm = "Добавление нового параметра";
            EnableChangeMeasureGroup = true;
            AddCommand = new RelayCommand(AddParameter, CanAdd);
        }

        private async void AddParameter(object obj)
        {
            Parametr parametr = new Parametr();
            parametr.Id = Guid.NewGuid();
            parametr.Code = ParameterCode;
            parametr.Description = ParameterName;
            parametr.MeasureGroup = SelectedMeasureGroup;
            parametr.ParameterGroup = SelectedParameterGroups;

            _paramRepBK.Add(parametr); //Запись

            ////Лютая дичь, для того, чтобы синхронизировать 2 БД - Зачем было их разделять???
            //EF.Repository.Repository.ParameterBKRepository rep1 = new EF.Repository.Repository.ParameterBKRepository();
            //EF.Repository.Korchagina.Repository.ParameterBKRepository rep2 = new EF.Repository.Korchagina.Repository.ParameterBKRepository();
            //EF.Repository.Rakushka.Repository.ParameterRepository rep3 = new EF.Repository.Rakushka.Repository.ParameterRepository();

            //rep1.Add(parametr);
            //rep2.Add(parametr);
            //rep3.Add(parametr);


            //В форму
            parametersList.Add(parametr);


            //Уведомление
            string project = _rc.ProjectSelectedName;
            await Task.Run(() => EmailSender.SendParamInfo(String.Format("Проект: {0}\r\nПользователь добавил новый Parameter:\r\n{1}",project, parametr.ToString())));



        }

        private bool CanAdd(object obj)
        {
            return SelectedMeasureGroup != null
                && SelectedParameterGroups != null
                && ParameterName != null && ParameterName != ""
                && ParameterCode != null && ParameterCode != "";
        }
    }
}
