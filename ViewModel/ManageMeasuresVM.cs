﻿using DataLayer;
using EF.Repository.Abstract.Common;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;

namespace ViewModel
{
    public class ManageMeasuresVM : ViewModelBase
    {
        private IMeasuresRepository _rep;
        private IMeasureGroupsRepository _repMeasGroup;
        private ObservableCollection<Measure> _measList;

        public ObservableCollection<Measure> MeasuresList
        {
            get { return _measList; }
            set
            {
                _measList = value;
                RaisePropertyChanged("MeasuresList");

            }
        }

        //Команды
        public ICommand AddCommand { get; private set; }


        public ManageMeasuresVM(GalaSoft.MvvmLight.Views.IDialogService dialogService, RepositoryContainer rc)
        {

            this._rep = rc._mesRepBK;
            this._repMeasGroup = rc._mesGrRep;
            MeasuresList = new ObservableCollection<Measure>();
            foreach (Measure meas  in _rep.SelectAll())
            {
                MeasuresList.Add(meas);
            }            

            AddCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(AddMeasure, CanAdd);


        }

        private bool CanAdd(object obj)
        {
            return true;
        }

        private void AddMeasure(object obj)
        {

            AddMeasureWindowVM addVM = new AddMeasureWindowVM(_rep, _repMeasGroup.SelectAll().ToList(), MeasuresList);
            //Показ формы добавления 
            Messenger.Default.Send(addVM, "ShowAddMeasureView");
            
        }
    }
}
