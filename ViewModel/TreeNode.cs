﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ViewModel
{
    public class TreeNode
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ObservableCollection<TreeNode> ChildNodes { get; set; }

        public TreeNode ParentNode { get; set; }


        public string FacilityCode { get; set; }

    }
}
