﻿using DataLayer;
using EF.Repository.Abstract.Common;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModel
{
    public class AddMeasureWindowVM : ViewModelBase
    {
        private IMeasuresRepository _rep;
        private ObservableCollection<Measure> MeasuresList;

        public List<MeasureGroup> MeasureGroups { get; }

        public MeasureGroup SelectedMeasureGroup { get; set; }

        public string MeasureCode { get; set; }
        public string MeasureRatio { get; set; }

        public string MeasureAbbreviation { get; set; }

        public string MeasureName { get; set; }

        public ICommand AddCommand { get; set; }

        

        public AddMeasureWindowVM(IMeasuresRepository rep, List<MeasureGroup> measureGroups, ObservableCollection <Measure> measuresList)
        {
            _rep = rep;
            MeasureGroups = measureGroups;
            MeasuresList = measuresList;
            AddCommand = new RelayCommand(AddMeasure, CanAdd);
        }

        private void AddMeasure(object obj)
        {
            Measure meas = new Measure();
            meas.Id = Guid.NewGuid();
            meas.Name = MeasureName;
            meas.Code = MeasureCode;
            meas.MeasureGroupId = SelectedMeasureGroup.Id;
            meas.MeasureGroup = SelectedMeasureGroup;
            meas.Abbreviation = MeasureAbbreviation;
            meas.Ratio = MeasureRatio;

            _rep.Add(meas); //Запись только в выбранный проект

            ////Лютая дичь, для того, чтобы синхронизировать 2 БД - Зачем было их разделять???
            //EF.Repository.Repository.MeasuresBKRepository rep1 = new EF.Repository.Repository.MeasuresBKRepository();
            //EF.Repository.Korchagina.Repository.MeasuresBKRepository rep2 = new EF.Repository.Korchagina.Repository.MeasuresBKRepository();
            //EF.Repository.Rakushka.Repository.MeasuresRepository rep3 = new EF.Repository.Rakushka.Repository.MeasuresRepository();

            //rep1.Add(meas);
            //rep2.Add(meas);
            //rep3.Add(meas);

            MeasuresList.Add(meas);
            
        }

        private bool CanAdd(object obj)
        {
            return MeasureName != null && MeasureName != "";
        }
    }
}
