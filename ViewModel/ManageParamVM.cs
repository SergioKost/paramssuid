﻿using DataLayer;
using DataLayer.Services;
using EF.Repository.Abstract;
using EF.Repository.Abstract.Common;
using EF.Repository.Repository;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;

namespace ViewModel
{
    public class ManageParamVM : ViewModelBase
    {
        //Репозитории
        IFacilityRepository _facRep;
        IParameterRepository _paramRep;
        IParameterRepository _paramRepBK;
        IMeasuresRepository _mesRep;
        IMeasuresRepository _mesRepBK;
        IElementRepository _elemRep;
        IElementParamRepository _elemParamRep;
        IElementParamRepository _elemParamRepBK;
        IStagesRepository _stageRep;
        private IMeasureGroupsRepository _measGrRep;

        //Для показа дочерних окон
        private GalaSoft.MvvmLight.Views.IDialogService _dialogService;
        
        private bool _isFreeIndicator;
        private ObservableCollection<TreeNode> _treeNodes;
        private TreeNode _selectedTreeNode;
        private ObservableCollection<Element> _elemList;
        private Element _selectedElem;
        private string _copyInfoText;

        //Список параметров для копирования
        private ObservableCollection<ElementParam> CopiedParamList { get; set; }
        private Guid? CopiedParamElemId { get; set; }

        //Текущий пользователь
        public User CurrentUser { get; private set; }

        //Список параметров
        public List<Parametr> ParamList { get; set; }

        //Список параметров для БК
        public List<Parametr> ParamList1 { get; set; }
        public List<Measure> MeasureList { get; set; }

        public List<Measure> MeasureList1 { get; set; }

        public List<Stage> StageList { get; set; }

        public ElementParam SelectedParamElem { get; set; }
        public List<ElementParam> SelectedParamElements { get; set; }
        public List<Element> SelectedElements { get; set; }

        //Команды
        public ICommand EditParaCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand AddParaCommand { get; set; }
        public ICommand CopyParaCommand { get; set; }
        public ICommand PasteParaCommand { get; set; }
        public ICommand IsCompleteCommand { get; set; }
        public ICommand SpecSetCommand { get; set; }
        public RelayCommand<IList> SelectionChangedCommand { get; private set; }
        public RelayCommand<IList> SelectionElemChangedCommand { get; private set; }

        public bool IsFreeIndicator
        {
            get { return _isFreeIndicator; }
            set
            {
                _isFreeIndicator = value;
                RaisePropertyChanged("IsFreeIndicator");
            }
        }

        public ObservableCollection<TreeNode> TreeNodes
        {
            get
            {
                return _treeNodes;
            }
            set
            {
                _treeNodes = value;
                RaisePropertyChanged("TreeNodes");
            }
        }
        public TreeNode SelectedTreeNode
        {
            get { return _selectedTreeNode; }
            set
            {
                _selectedTreeNode = value;
                //Определяем новый объект
                bool IsSelSystemBCNew = false;
                if (value != null)
                {
                    IsSelSystemBCNew = DataProvider.GetFacilitiesCodesForBKTables().Contains(value.FacilityCode);
                }          

                //Если поменялся               
                if (IsSelSystemBCNew != IsSelSystemBC)
                {
                    IsSelSystemBC = IsSelSystemBCNew;

                    //Очищаем скопированные параметры
                    CopiedParamElemId = null;
                    CopiedParamList = null;
                    CopyInfoText = null;
                }

                IsFreeIndicator = false;
                new Microsoft.TeamFoundation.MVVM.RelayCommand(async () =>
                {

                    //Запуск крутилки

                    //Получение элементов
                    ElemList = await SelectedTreeNodeChanged();
                    IsFreeIndicator = true;
                }).Execute(null);

            }
        }
        public ObservableCollection<Element> ElemList
        {
            get
            {
                return _elemList;
            }
            set
            {
                _elemList = value;
                RaisePropertyChanged("ElemList");
            }
        }
        public Element SelectedElem
        {
            get
            {
                return _selectedElem;
            }
            set
            {
                _selectedElem = value;
                RaisePropertyChanged("SelectedElem");
            }
        }

        //Переменная, показывающая выбран ли БК (т.к. в этом случае мы используем другие параметры)
        public bool IsSelSystemBC { get; set; }


        public string CopyInfoText
        {
            get
            {
                return _copyInfoText;
            }
            set
            {
                _copyInfoText = value;
                RaisePropertyChanged("CopyInfoText");
            }

        }


        public string VersionNumber
        {
            get
            {
                return VersionFactory.GetCurVersion();
            }
        }

        //Конструктор
        //public ManageParamVM(IWindowService WindowFactory)
        public ManageParamVM(GalaSoft.MvvmLight.Views.IDialogService dialogService, RepositoryContainer rc)
        {

            Logger.Logger.Info("ManageParamVM creating...");

            _dialogService = dialogService;

            //Проверка доступа
            if (!UserService.CheckAccess(rc._userRep))
            {
                //Сообщение
                _dialogService.ShowError("Текущий пользователь не найден в таблице User. Обратитесь к системному администратору",
                                         "Ошибка доступа", 
                                         buttonText:"Ok", null);
                
                 IsFreeIndicator = true;
                 return;
            }
            else
            {
                //установка текущего пользователя
                CurrentUser = rc._userRep.SelectByName(DataLayer.Services.DataProvider.GetUserName()).First();
            }


            //Передача репозиториев
            _facRep = rc._facRep;
            _paramRep = rc._paramRep;
            _paramRepBK = rc._paramRepBK;
            _mesRep = rc._mesRep;
            _mesRepBK = rc._mesRepBK;
            _elemRep = rc._elemRep;
            _elemParamRep = rc._elemParamRep;
            _elemParamRepBK = rc._elemParamRepBK;
            _stageRep = rc._stageRep;
            _measGrRep = rc._mesGrRep;

            //Получаем параметры            
            ParamList =  _paramRep?.SelectAll().ToList();
            Comparator<Parametr> cmp = new Comparator<Parametr>();
            ParamList?.Sort(cmp);

            //Получаем параметры для БК            
            ParamList1 = _paramRepBK?.SelectAll().ToList();
            ParamList1?.Sort(cmp);
            
            //Получаем ед.изм.            
            MeasureList = _mesRep?.SelectAll().ToList();
            Comparator<Measure> cmp2 = new Comparator<Measure>();
            MeasureList?.Sort(cmp2);

            //Получаем ед.изм. для БК
            MeasureList1 = _mesRepBK?.SelectAll().ToList();
            MeasureList1?.Sort(cmp2);


            //Перечень возможных стадий
            StageList = _stageRep.SelectAll().ToList();

            EditParaCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(EditParam, CanEdit);
            AddParaCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(AddParam, CanAdd);
            DeleteCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(DelParam, CanDel);
            CopyParaCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(CopyParam, CanCopy);
            PasteParaCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(PasteParam, CanPaste);
            IsCompleteCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(SetComplete, CanSetComplete);
            SpecSetCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(SetSpec, CanSetSpec);

            SelectionChangedCommand = new GalaSoft.MvvmLight.Command.RelayCommand<IList>(
                items =>
                {
                    SelectedParamElements = new List<ElementParam>();
                    if (items != null)
                    {
                        foreach (var item in items)
                        {
                            SelectedParamElements.Add((ElementParam)item);
                        }
                    }

                    return;
                });

            SelectionElemChangedCommand = new GalaSoft.MvvmLight.Command.RelayCommand<IList>(
                items =>
                {
                    SelectedElements = new List<Element>();
                    if (items != null)
                    {
                        foreach (var item in items)
                        {
                            SelectedElements.Add((Element)item);
                        }
                    }
                    return;
                });

            ElemList = new ObservableCollection<Element>();
            TreeNodes = new ObservableCollection<TreeNode>();
            
           

            Messenger.Default.Register<NotificationMessage>(this, "ReturnedSetCompleteValue", msg =>
            {
                SetCompleteValue(msg.Notification);
            });



            IsFreeIndicator = true;

            //Получаем дерево асинхронно
            this.Dispatcher.Invoke((Action)delegate { LoadTreeAsync(_facRep); });
        }

        

        private void SetCompleteValue(string notification)
        {
            try
            {
                //обновляем все выделенные элементы
                //foreach (Element elemItem in SelectedElements)
                //Обновляем выделенный элемент
                Element elemItem = SelectedElem;
                {
                    Element elEditor = ElemList.FirstOrDefault(x => (x.Id == elemItem.Id));
                    elEditor.IsComplete = notification;               

                    //Сохранение в БД
                    _elemRep.SetIsComplete(elemItem, notification);
                    //Обновление формы
                    int index = ElemList.IndexOf(elEditor);
                    ElemList[index] = _elemRep.Select(elemItem.Id);
                    //Обновляем IsBKElement
                    ElemList[index].IsBKElement = IsSelSystemBC;
                }

                Logger.Logger.Info("Attribute IsComplete successfully set");

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(ex, "Error on SetComplete");
                throw;
            }
        }

        private async void LoadTreeAsync(IFacilityRepository repFac)
        {
            TreeNodes = await LoadTree(repFac);
        }

        private Task<ObservableCollection<TreeNode>> LoadTree(IFacilityRepository repFac)
        {
            ObservableCollection<TreeNode> result = new ObservableCollection<TreeNode>();

            return Task.Run(() =>
            {

                List<Facility> facList = new List<Facility>();
                facList = repFac.SelectAll().ToList();
                
                foreach (Facility fac in facList)
                {
                    TreeNode tnf = new TreeNode() { Id = fac.Id, Name = fac.Code + " - " + fac.Name, FacilityCode = fac.Code };
                    if (fac.Complexes != null)
                    {
                        tnf.ChildNodes = new ObservableCollection<TreeNode>();
                        foreach (Complex complex in fac.Complexes)
                        {
                            TreeNode tnc = new TreeNode() { Id = complex.Id, Name = complex.Code + " - " + complex.Name, FacilityCode = fac.Code };
                            if (complex.Systems != null)
                            {
                                tnc.ChildNodes = new ObservableCollection<TreeNode>();
                                foreach (DataLayer.System system in complex.Systems)
                                {
                                    TreeNode tns = new TreeNode() { Id = system.Id, Name = system.Code + " - " + system.Name, FacilityCode = fac.Code };
                                    tnc.ChildNodes.Add(tns);
                                }

                                //Сортировка систем
                                tnc.ChildNodes = new ObservableCollection<TreeNode>(tnc.ChildNodes.OrderBy(x => x.Name));
                            }
                            tnf.ChildNodes.Add(tnc);
                        }
                        //Сортировка комплексов
                        tnf.ChildNodes = new ObservableCollection<TreeNode>(tnf.ChildNodes.OrderBy(x => x.Name));

                    }
                    result.Add(tnf);
                }
                return result;
            });
        }

        private bool CanDel(object obj)
        {
            return SelectedParamElem != null;
        }

        //Удаление
        private void DelParam(object obj)
        {
            try
            {
                string text = "";
                if (SelectedParamElements.Count == 1)
                {
                    text = String.Format("Вы действительно хотите удалить параметр \"{0}\"?", SelectedParamElem.Description);
                }
                else
                {
                    text = String.Format("Вы действительно хотите удалить {0} параметра(ов) элемента {1} - {2}?", SelectedParamElements.Count, SelectedElem.Position, SelectedElem.Name);
                }

                _dialogService.ShowMessage(text, "Confirm", "Да", "Нет", (confirmed) => {
                    if (confirmed)
                    {
                        if (IsSelSystemBC)
                        {
                            foreach (ElementParam item in SelectedParamElements)
                            {
                                _elemParamRepBK.Delete(item);
                            }


                        }
                        else
                        {
                            foreach (ElementParam item in SelectedParamElements)
                            {
                                _elemParamRep.Delete(item);
                            }
                        }

                        //Из формы
                        foreach (ElementParam item in SelectedParamElements)
                        {
                            SelectedElem.ParamList.Remove(item);
                        }

                        Logger.Logger.Info("Param(s) successfully deleted");
                    }                    
                    
                });               

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(ex, "Error on DelParam");
                throw;
            }

        }

        public bool CanEdit(object t)
        {
            return SelectedParamElem != null;
        }

        public void EditParam(object t)
        {
            try
            {
                EditWindowVM editVM;
                SelectedParamElem.ListStage = StageList;

                //var tt6 = _measGrRep.SelectAll();
                //var tt5 = SelectedParamElem.Measure.MeasureGroupId;

                //Guid measGrId = _measGrRep.SelectAll()
                //    .Where(v => v.Id == SelectedParamElem.Measure.MeasureGroupId)
                //    .FirstOrDefault().Id;

                ElementParam editapleSelectedElemParam = (ElementParam)SelectedParamElem.Clone();
                editapleSelectedElemParam.Id = SelectedParamElem.Id;
                IElementParamRepository repForEdit;            

                if (IsSelSystemBC)
                {
                    editapleSelectedElemParam.ListMeasure = _mesRepBK.SelectByParameterId(editapleSelectedElemParam.ParameterId).ToList();
                    repForEdit = _elemParamRepBK;
                    //editVM = new EditWindowVM(editapleSelectedElemParam, SelectedElem, _elemParamRepBK) { TitleForm = "Редактировать" };
                }
                else
                {
                    editapleSelectedElemParam.ListMeasure = _mesRep.SelectByParameterId(editapleSelectedElemParam.ParameterId).ToList();
                    repForEdit = _elemParamRep;
                    //editVM = new EditWindowVM(editapleSelectedElemParam, SelectedElem, _elemParamRep) { TitleForm = "Редактировать" };
                }

                if (editapleSelectedElemParam.Measure != null)
                {
                    editapleSelectedElemParam.Measure = editapleSelectedElemParam.ListMeasure.FirstOrDefault(x => (x.Id == editapleSelectedElemParam.Measure.Id));
                }

                if (editapleSelectedElemParam.Stage != null)
                {
                    editapleSelectedElemParam.Stage = editapleSelectedElemParam.ListStage.FirstOrDefault(x => (x.Id == editapleSelectedElemParam.Stage.Id));
                }


                editVM = new EditWindowVM(editapleSelectedElemParam, SelectedElem, repForEdit) { TitleForm = "Редактировать" };

                Messenger.Default.Send(editVM, "ShowEditView");               




                Logger.Logger.Info("Param successfully edited");

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(ex, "Error on EditParam");
                throw;
            }

        }

        public void AddParam(object t)
        {
            try
            {
                AddWindowVM addVM;
                //string DefaultStageName = "Э";
                //В зависимости от выбранного объекта
                if (IsSelSystemBC)
                {

                    addVM = new AddWindowVM(SelectedElem, ParamList1, _elemParamRepBK, _stageRep, _mesRepBK) { TitleForm = "Добавить параметр БК" };
                }
                else
                {
                    addVM = new AddWindowVM(SelectedElem, ParamList, _elemParamRep, _stageRep, _mesRep) { TitleForm = "Добавить параметр" };
                }
                                
                //Показ формы редактиривания                
                Messenger.Default.Send(addVM, "ShowAddView");                

                //обновляем текущий элемент
                //Заново получаем текущий элемент
                Element newElem = _elemRep.Select(SelectedElem.Id);
                SelectedElem.ParamList = newElem.ParamList;
                RaisePropertyChanged("SelectedElem");

                Logger.Logger.Info("Param successfully added");

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(ex, "Error on AddParam");
                throw;
            }


        }

        private bool CanAdd(object obj)
        {
            return SelectedElem != null;
        }

        public void CopyParam(object t)
        {
            try
            {
                //Запоминаем параметры
                CopiedParamList = new ObservableCollection<ElementParam>();
                CopiedParamElemId = SelectedElem.Id;
                
                //Копирование выделенных параметров
                foreach (ElementParam item in SelectedParamElements)
                {
                    CopiedParamList.Add((ElementParam)item.Clone());
                }

                //выводим информацию
                CopyInfoText = String.Format("Скопировано {0} параметров элемента {1} - {2}", CopiedParamList.Count, SelectedElem.Position, SelectedElem.Name);

                Logger.Logger.Info(CopyInfoText);

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(ex, "Error on CopyParam");
                throw;
            }

        }

        private bool CanCopy(object obj)
        {
            return (SelectedParamElements != null && SelectedParamElements.Count > 0);
        }

        public void PasteParam(object t)
        {
            string text = String.Format("Вы действительно хотите добавить {0} параметров к элементу {1}?", CopiedParamList.Count, SelectedElem.Position);

            try
            {
                _dialogService.ShowMessage(text, "Confirm", "Да", "Нет", (confirmed) => {
                    if (confirmed)
                    {
                        if (IsSelSystemBC)
                        {
                            foreach (ElementParam item in CopiedParamList)
                            {
                                _elemParamRepBK.AddCopy(item, SelectedElem);
                            }
                        }
                        else
                        {
                            foreach (ElementParam item in CopiedParamList)
                            {
                                _elemParamRep.AddCopy(item, SelectedElem);
                            }
                        }

                        //В форму
                        //Заново получаем текущий элемент
                        Element newElem = _elemRep.Select(SelectedElem.Id);
                        SelectedElem.ParamList = newElem.ParamList;
                        RaisePropertyChanged("SelectedElem");
                        Logger.Logger.Info("Param(s) successfully pasted");
                    }

                });
            }
            catch (Exception ex)
            {
                string textErr = String.Format("Что-то пошло не так при копировании параметров.Ошибка:{0},{1},{2}, {3}", ex.Message, ex.TargetSite, ex.Source, ex.StackTrace);
                _dialogService.ShowError(textErr, "Ошибка", "ОК", null);
                Logger.Logger.Error(ex, "Error on PasteParam");
            }           
        }

        private bool CanPaste(object obj)
        {
            //return (SelectedElem != null && CopiedParamList != null && SelectedElem.Id != CopiedParamElemId);
            return SelectedElem != null && CopiedParamList != null;
        }

        public void SetComplete(object t)
        {
            //Запуск формы для SetComplete
            Messenger.Default.Send<NotificationMessage>(null, "ShowSetCompleteView");

        }
        private bool CanSetComplete(object obj)
        {
            return (SelectedElem != null);
        }

        public void SetSpec(object t)
        {
            try
            {                

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(ex, "Error on SetSpec");
                throw;
            }

        }
        private bool CanSetSpec(object obj)
        {
            //return (SelectedElem != null);
            return false;
        }

        private Task<ObservableCollection<Element>> SelectedTreeNodeChanged()
        {
            //MessageBox.Show(String.Format("В дереве выбран элемент: id = {0}, Name = {1}", SelectedTreeNode.Id, SelectedTreeNode.Name));
            return Task<ObservableCollection<Element>>.Run(() =>
            {
                ObservableCollection<Element> result = new ObservableCollection<Element>();
                if (SelectedTreeNode != null)
                {


                    foreach (var item in _elemRep.SelectBySystem(SelectedTreeNode.Id))
                    {
                        //ElemList.Add(item);
                        item.IsBKElement = IsSelSystemBC;
                        result.Add(item);
                    }
                }
                return result;
            });
        }

    }
}
