﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Services;
using EF.Repository.Abstract.Common;

namespace ViewModel.Services
{
    public static class UserService
    {
        //public static string GetCurrentName()
        //{
        //    return Environment.UserName;
        //}        

        internal static bool CheckAccess(IUserRepository userRep)
        {
            return userRep.SelectByName(DataLayer.Services.DataProvider.GetUserName()).Count > 0;
            
        }

        internal static bool CheckAdmin(IUserRepository userRep)
        {
            string userName = DataLayer.Services.DataProvider.GetUserName();
            var users = userRep.SelectByName(userName);
            if (users.Count > 0)
            {
                return users.First().IsAdmin;
            }
            return false;
        }
    }



}
