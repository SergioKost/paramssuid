﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.ComponentModel;


namespace ViewModel.Services
{
    public enum ListAddressees
    {
        [Description("epaderikha@vnp.ltd")]
        Aderikha,
        [Description("sakostenko@vnp.ltd")]
        Kostenko
    }
    public static class EmailSender
    {
        

        public static void SendParamInfo(string message)
        {
            var outlApp = new Application();
            var mail = outlApp.CreateItem(OlItemType.olMailItem) as MailItem;            
            mail.Subject = "Изменения данных в параметрах";
            mail.Body = message;            
            Account account;
            foreach (Account itemAcc in outlApp.Session.Accounts)
            {
                if (itemAcc.SmtpAddress.Contains("@mtech.ltd"))
                {
                    account = itemAcc;
                    mail.SendUsingAccount = account;
                    mail.To = ListAddressees.Kostenko.ToDescriptionString() + ";" + ListAddressees.Aderikha.ToDescriptionString();
                    mail.Send();                   
                    break;
                }
            }  

        }
    }



    public static class MyEnumExtensions
    {
        public static string ToDescriptionString(this ListAddressees val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }

}