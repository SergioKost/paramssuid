﻿using EF.Repository.Abstract.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Services
{
    public class RepositoryContainer
    {

        public string ProjectSelectedName { get; private set; }
        public int ProjectId { get; private set; }


        public IFacilityRepository _facRep { get; private set; }
        public IParameterRepository _paramRep { get; private set; }
        public IParameterRepository _paramRepBK { get; private set; }
        public IMeasuresRepository _mesRep { get; private set; }
        public IMeasuresRepository _mesRepBK { get; private set; }

        public IMeasureGroupsRepository _mesGrRep { get; private set; }

        public IParametersGroupsRepository _paramGrRep { get; private set; }

        public IElementRepository _elemRep { get; private set; }
        public IElementParamRepository _elemParamRep { get; private set; }
        public IElementParamRepository _elemParamRepBK { get; private set; }

        public IStagesRepository _stageRep { get; private set; }

        public ISubParamRepository _subParamRepBK { get; private set; }
        public ISubParamRepository _subParamRep { get; private set; }

        public IUserRepository _userRep { get; private set; }


        public RepositoryContainer(int mode)
        {
            switch (mode)
            {
                case 0: //Репозитории для Филановк
                    ProjectSelectedName = "Филановка";
                    ProjectId = 0;
                    _facRep = new EF.Repository.Repository.FacilityRepository();
                    _paramRep = new EF.Repository.Repository.ParameterRepository();
                    _paramRepBK = new EF.Repository.Repository.ParameterBKRepository();
                    _mesRep = new EF.Repository.Repository.MeasuresRepository();
                    _mesRepBK = new EF.Repository.Repository.MeasuresBKRepository();
                    _mesGrRep = new EF.Repository.Repository.MeasureGroupsRepository();
                    _paramGrRep = new EF.Repository.Repository.ParametersGroupsRepository();
                    _elemRep = new EF.Repository.Repository.ElementRepository();
                    _elemParamRep = new EF.Repository.Repository.ElementParamRepository();
                    _elemParamRepBK = new EF.Repository.Repository.ElementsParamBKRepository();
                    _stageRep = new EF.Repository.Repository.StagesRepository();
                    _subParamRepBK = new EF.Repository.Repository.SubParamBKRepository();
                    _userRep = new EF.Repository.Repository.UserRepository();
                    break;
                case 1: //Репозитории для Корчагина
                    ProjectSelectedName = "Корчагина";
                    ProjectId = 1;
                    _facRep = new EF.Repository.Korchagina.Repository.FacilityRepository();
                    _paramRep = new EF.Repository.Korchagina.Repository.ParameterRepository();
                    _paramRepBK = new EF.Repository.Korchagina.Repository.ParameterBKRepository();
                    _mesRep = new EF.Repository.Korchagina.Repository.MeasuresRepository();
                    _mesRepBK = new EF.Repository.Korchagina.Repository.MeasuresBKRepository();
                    _elemRep = new EF.Repository.Korchagina.Repository.ElementRepository();
                    _elemParamRep = new EF.Repository.Korchagina.Repository.ElementParamRepository();
                    _elemParamRepBK = new EF.Repository.Korchagina.Repository.ElementsParamBKRepository();
                    _stageRep = new EF.Repository.Korchagina.Repository.StagesRepository();
                    _mesGrRep = new EF.Repository.Korchagina.Repository.MeasureGroupsRepository();
                    _subParamRepBK = new EF.Repository.Korchagina.Repository.SubParamBKRepository();
                    _paramGrRep = new EF.Repository.Korchagina.Repository.ParametersGroupsRepository();
                    _userRep = new EF.Repository.Korchagina.Repository.UserRepository();
                    break;
                case 2: //Репозитории для Ракушки
                    ProjectSelectedName = "Грайфера";
                    ProjectId = 2;
                    _facRep = new EF.Repository.Rakushka.Repository.FacilityRepository();
                    _paramRepBK = new EF.Repository.Rakushka.Repository.ParameterRepository();
                    _paramRep = new EF.Repository.Rakushka.Repository.ParameterRepository();
                    
                    _mesRepBK = new EF.Repository.Rakushka.Repository.MeasuresRepository();
                    _mesRep = new EF.Repository.Rakushka.Repository.MeasuresRepository();
                    
                    _elemRep = new EF.Repository.Rakushka.Repository.ElementRepository();
                    
                    _elemParamRepBK = new EF.Repository.Rakushka.Repository.ElementsParamRepository();
                    _elemParamRep = new EF.Repository.Rakushka.Repository.ElementsParamRepository();
                    
                    _stageRep = new EF.Repository.Rakushka.Repository.StagesRepository();
                    _mesGrRep = new EF.Repository.Rakushka.Repository.MeasureGroupsRepository();
                    
                    _subParamRepBK = new EF.Repository.Rakushka.Repository.SubParamRepository();
                    _subParamRep = new EF.Repository.Rakushka.Repository.SubParamRepository();
                    
                    _paramGrRep = new EF.Repository.Rakushka.Repository.ParametersGroupsRepository();
                    _userRep = new EF.Repository.Rakushka.Repository.UserRepository();
                    break;
                default:
                    break;
            }
}
    }
}
