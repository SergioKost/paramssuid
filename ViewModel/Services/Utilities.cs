﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Services
{
    public static class Utilities
    {       

        internal static bool CheckCode(string subParameterCode, string subParameterName, Parametr param, RepositoryContainer rc)
        {
            string[] ids = Properties.Resources.ParamOrganizationId.Split(';');
            bool find = false;
            Guid findId = Guid.Empty;
            for (int i = 0; i < ids.Length; i++)
            {
                findId = Guid.Parse(ids[i]);
                if (param.Id == findId)
                { 
                    find = true;
                    break;
                }
            }
            if (find && rc.ProjectId == 2)   //Если SubParameter к параметрам органицаций (Завод-изготовитель или Поставщик) для Ракушки
            {
                var findDublCodeName = rc._subParamRepBK.SelectByCodeName(subParameterCode, subParameterName);
                if (findDublCodeName.Count == 1 && findId != findDublCodeName.First().ParameterId)
                {
                    return false; //Дублироваться может только 1 раз и только когда совпадают и код, и наименование, и из другого параметра
                }
            }
            //Проверка на дублирование остальных subparametr - не организаций;
            return rc._subParamRepBK.SelectByCode(subParameterCode).Count > 0;
        }
    }
}
