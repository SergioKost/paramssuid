﻿using DataLayer;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.TeamFoundation.MVVM;

using EF.Repository.Abstract.Common;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Services;
using System.Collections.ObjectModel;

namespace ViewModel
{
    public class ManageSubParametersVM : ViewModelBase
    {
        private GalaSoft.MvvmLight.Views.IDialogService _dialogService;
        private readonly RepositoryContainer _rc;

        public List<Parametr> ParamList { get; set; }

        private ISubParamRepository _rep;
        private IParameterRepository _repParam;


        private Parametr _selectedParam;
        
        public Parametr SelectedParam
        {
            get { return _selectedParam; }
            set
            {
                _selectedParam = value;
                RaisePropertyChanged("SelectedParam");
            }
        }

        public SubParam SelectedSubParam { get; set; }


        private ObservableCollection<SubParam> _subParamList;

        public ObservableCollection<SubParam> SubParamList
        {
            get { return _subParamList; }
            set
            {
                _subParamList = value;
                RaisePropertyChanged("SubParamList");

            }
        }

        //Команды
        public ICommand AddCommand { get; private set; }

        public ICommand EditCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }


        public ManageSubParametersVM(GalaSoft.MvvmLight.Views.IDialogService dialogService, RepositoryContainer rc)
        {
            _dialogService = dialogService;
            this._rc = rc;
            this._rep = rc._subParamRepBK;
            this._repParam = rc._paramRepBK;

            ParamList = rc._paramRepBK.SelectAll()
                .Where(t => t.MeasureGroup.Name == "Элемент списка")
                .ToList();


            AddCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(ShowAddForm, CanAdd);
            EditCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(ShowEditForm, CanEdit);
            DeleteCommand = new Microsoft.TeamFoundation.MVVM.RelayCommand(DeleteSubParam, CanDelete);

        }

        private void DeleteSubParam(object obj)
        {
            string text = $"Вы действительно хотите удалить параметр \"{SelectedSubParam.Name}\"?";            
            _dialogService.ShowMessage(text, "Confirm", "Да", "Нет", async (confirmed) => {
                if (confirmed)
                {
                    //Из БД
                    bool result = _rep.Delete(SelectedSubParam);
                    //Из формы
                    if (result)
                    {
                        Logger.Logger.Info($"SubParam \"{SelectedSubParam.Name}\" successfully deleted");
                        SelectedParam.SubParameters.Remove(SelectedSubParam);
                        _dialogService.ShowMessageBox("SubParameter удален", "Информация");

                        //Уведомление
                        string project = _rc.ProjectSelectedName;
                        await Task.Run(() => EmailSender.SendParamInfo(String.Format("Проект: {0}\r\nПользователь удалил SubParameter:\r\n{1}", project, SelectedSubParam.ToString())));



                    }
                    else
                    {
                        _dialogService.ShowMessageBox("Невозможно удалить", "Информация");
                    }
                    
                }

            });
        }

        private bool CanDelete(object obj)
        {
            return SelectedSubParam != null;
        }

        private bool CanEdit(object obj)
        {
            return SelectedSubParam != null;
        }

        private void ShowEditForm(object obj)
        {

            SubParam editSubParam = new SubParam();
            editSubParam = (SubParam)SelectedSubParam.Clone();

            EditSubParamWindowVM editVM = new EditSubParamWindowVM(editSubParam, SelectedParam,_rc);
            //Показ формы редактирования 
            Messenger.Default.Send(editVM, "ShowEditSubParamView");


        }

        private bool CanAdd(object obj)
        {
            return SelectedParam != null;
        }

        private void ShowAddForm(object obj)
        {
            AddSubParamWindowVM addVM = new AddSubParamWindowVM(SelectedParam, _rc);
            //Показ формы добавления 
            Messenger.Default.Send(addVM, "ShowAddSubParamView");


            //Обновление формы
            //SelectedParam.SubParameters = _rep.SelectByParameterId(SelectedParam.Id);
        }
    }
}
