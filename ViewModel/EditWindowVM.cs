﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.TeamFoundation.MVVM;
using DataLayer;
using EF.Repository.Repository;
using EF.Repository.Abstract.Common;

namespace ViewModel
{
    public class EditWindowVM : ViewModelBase
    {
        private readonly IElementParamRepository _rep;

        public Action CloseAction { get; set; }

        public string TitleForm { get; set; }

        public ICommand SaveCommand { get; set; }
        public ICommand ValidateCommand { get; set; }
        
        public ElementParam Param { get; set; }

        public Element element { get; set; }

        public bool IsListParam { get; set; }
        public bool IsStringParam { get; set; }

        public bool IsValidate { get; set; }

        public EditWindowVM(ElementParam p, Element SelElem, IElementParamRepository repository)
        {
            SaveCommand = new RelayCommand(SaveUpdate, CanSaveUpdate);
            ValidateCommand = new RelayCommand(Validate, (object t) => { return true; });
            
            Param = p;
            element = SelElem;
            this._rep = repository;
            IsListParam = (Param.ListValue != null);
            IsStringParam = (Param.ListValue == null);
        }

        private void Validate(object eventArg)
        {
            IsValidate = ElementParam.ValidateValue((string)eventArg);
            RaisePropertyChanged("IsValidate");
        }

        private bool CanSaveUpdate(object obj)
        {
            if (IsStringParam)
            {
                return (Param.Value != null 
                    && Param.Value != ""
                    && Param.Id != null
                    && Param.Stage != null
                    && IsValidate) ;
            }
            else
            {
                return (Param.SubParamValue != null && Param.Id != null && Param.Stage != null);
            }
 
        }

        private void SaveUpdate(object obj)
        {
            _rep.Update(Param);


            //Замена старого параметра для отображения в форме
            ElementParam oldParam = element.ParamList.Where(x => x.Id == Param.Id).FirstOrDefault();
            int replacedIndex = element.ParamList.IndexOf(oldParam);
            element.ParamList[replacedIndex] = Param;

            CloseAction();
            
        }

      
    }
}
