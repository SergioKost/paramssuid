﻿using Microsoft.TeamFoundation.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using EF.Repository.Abstract.Common;
using System.Windows.Input;
using ViewModel.Services;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using ViewModelBase = Microsoft.TeamFoundation.MVVM.ViewModelBase;
using System.ComponentModel;

namespace ViewModel
{
    public class EditParameterWindowVM : ViewModelBase, IDataErrorInfo
    {
        private Parametr editableParam;
        private readonly RepositoryContainer _rc;

        //private Parametr selectedParam;
        private IParameterRepository rep;

        public List<MeasureGroup> ListMeasureGroups { get; }
        public MeasureGroup SelectedMeasureGroup { get; set; }

        public List<ParameterGroup> ListParameterGroups { get; }
        public ParameterGroup SelectedParameterGroups { get; set; }

        public string ParameterName { get; set; }

        public string ParameterCode { get; set; }


        public string TitleForm { get; set; }
        public string ButtonText { get; set; }
        public bool EnableChangeMeasureGroup { get; }


        public ICommand AddCommand { get; set; }
        public ObservableCollection<Parametr> ListParameters { get; private set; }

        public string Error => throw new NotImplementedException();

        public string this[string columnName]
        {
            get
            {
                string parameterCodeProp = BindableBase.GetPropertyName(() => ParameterCode);
                if (columnName == parameterCodeProp)
                    if (CheckCode(ParameterCode) && ParameterCode != editableParam.Code)
                        return "Код дублируется в базе!";
                return null;
            }
        }

        private bool CheckCode(string parameterCode)
        {
            //Проверка на дублирование;
            return rep.SelectByCode(parameterCode).Count > 0;
        }

        public EditParameterWindowVM(Parametr editParam, RepositoryContainer rc, ObservableCollection<Parametr> ListParameters)
        {
            this.editableParam = editParam;
            this._rc = rc;
            this.rep = rc._paramRepBK;
            this.ListParameters = ListParameters;
            ListMeasureGroups = editableParam.ListMeasureGroup;
            SelectedMeasureGroup = ListMeasureGroups.Where(x => x.Id == editableParam.MeasureGroup.Id).FirstOrDefault();
            ListParameterGroups = editableParam.ListParameterGroup;
            SelectedParameterGroups = ListParameterGroups.Where(x => x.Id == editableParam.ParameterGroup.Id).FirstOrDefault();
            ParameterName = editableParam.Description;
            ParameterCode = editableParam.Code;

            ButtonText = "Обновить параметр";
            TitleForm = "Обновление параметра: " + editableParam.Code;
            EnableChangeMeasureGroup = false;
            AddCommand = new RelayCommand(UpdateParameter, CanUpdate);
        }

        private bool CanUpdate(object obj)
        {
            return SelectedMeasureGroup != null
                && SelectedParameterGroups != null
                && ParameterName != null && ParameterName != ""
                && ParameterCode != null && ParameterCode != "";
        }

        private async void UpdateParameter(object obj)
        {
            editableParam.Code = ParameterCode;
            editableParam.Description = ParameterName;
            editableParam.MeasureGroup = SelectedMeasureGroup;
            editableParam.ParameterGroup = SelectedParameterGroups;

            rep.Update(editableParam);

            //В форму
            var item = ListParameters.FirstOrDefault(x => x.Id == editableParam.Id);
            int i = ListParameters.IndexOf(item);
            ListParameters[i] = editableParam;


            //Уведомление
            string project = _rc.ProjectSelectedName;
            await Task.Run(() => EmailSender.SendParamInfo(String.Format("Проект: {0}\r\nПользователь добавил новый Parameter:\r\n{1}",project, editableParam.ToString())));



        }
    }
}
